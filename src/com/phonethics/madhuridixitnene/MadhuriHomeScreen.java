package com.phonethics.madhuridixitnene;

import java.lang.reflect.Field;
import java.util.ArrayList;



import com.flurry.android.FlurryAgent;
import com.parse.PushService;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;


//import com.sbstrm.appirater.Appirater;

public class MadhuriHomeScreen extends FragmentActivity {

	CustomViewPager bottomPager;
	CustomViewPager mainPager;
	Context context;
	boolean b=false;
	private  int SWIPE_MIN_DISTANCE;
	private  int SWIPE_THRESHOLD_VELOCITY = 100;
	GestureDetector gdt= new GestureDetector(new GestureListener());

	private int mCurrentPage=0,mMainCurrentPage=0;

	private int mSelectedCurrentPage=0,mainSelectedPage;
	RelativeLayout relPager;
	LayoutParams params;
	private int sliderheight;


	private int SCROLLED=0;

	ArrayList<String> name,btnName,mainScreenImag,animate;

	HorizontalScrollView hsrcl1;

	View imgLatest,imgDance,imgMovies,imgFashion,imgHealth,imgArt,imgGallery;

	static boolean count=false;
	int changeImageCounter=0;
	private int scwidth;
	private int scheight;


	int bottomscrollPosition=0;
	int tapCounter=1;

	int scrollPosition=0; //Scrolled Position of BottomScroller
	Animation anim;

	boolean touch=false;

	RelativeLayout relmainMenu,rellaymainpgr;
	private static int isMenuTextShown=0;
	ImageView mainInfoText,main_menu,fbicon,twtricon,tapToEnter;


	IntentFilter intentFilter;



	/*	BroadCast broadCast=new BroadCast();*/

	final boolean TOAST=false;
	private float DISTANCEX=0,DISTANCEXX=0;

	private boolean isScrollingLeft=false;

	private boolean isSetteling=false;
	MainFixedSpeedScroller scroller2;

	public int stateOfPager;
	public int lastState=0;
	public int mainScreenPage;




	/*
	 * 
	 * Background Image
	 */

	ImageView imgBack;
	boolean fadeFlag;
	boolean clickedFlag=false;
	int scrolledValue=0;

	FixedSpeedScroller scroller;

	MediaPlayer tapSound;

	int constantscroll=0;
	int constantscrolled=0;

	boolean isPageChanging=true;
	boolean icFade=true;
	boolean isICScrolling=false;
	MainAdapter madpater;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_madhuri_home_screen);
		context=this;
		/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/

		/*	intentFilter = new IntentFilter(getPackageName()+getLocalClassName());
		 */

		/*
		 * rate my app
		 */
		//Appirater.appLaunched(this);

		DisplayMetrics metrics = this.getResources().getDisplayMetrics();
		scwidth = metrics.widthPixels;
		scheight = metrics.heightPixels;

	

		/*DisplayMetrics metrics2 = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics2);
		switch(metrics2.densityDpi){
		case DisplayMetrics.DENSITY_LOW:
			Toast.makeText(context, "Density - Low", Toast.LENGTH_LONG).show();
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			Toast.makeText(context, "Density - Medium", Toast.LENGTH_LONG).show();
			break;
		case DisplayMetrics.DENSITY_HIGH:
			Toast.makeText(context, "Density - High", Toast.LENGTH_LONG).show();
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			Toast.makeText(context, "Density - X-High", Toast.LENGTH_LONG).show();
			break;
		}*/

		//	Toast.makeText(context, "Screen Width"+scwidth+" Height "+scheight, Toast.LENGTH_SHORT).show();
		/*
		 * Sound on Click
		 */
		tapSound=MediaPlayer.create(getApplicationContext(), R.raw.i_button_sound);


		/*
		 * Minimum Distance that needs to Swipe to change view
		 */
		SWIPE_MIN_DISTANCE=(scwidth/2)-10;

		imgBack=(ImageView)findViewById(R.id.imgBack);

		relPager=(RelativeLayout)findViewById(R.id.relBottom);
		rellaymainpgr=(RelativeLayout)findViewById(R.id.relMain);

		relmainMenu=(RelativeLayout)findViewById(R.id.menulayout);
		relmainMenu.bringToFront();

		main_menu=(ImageView)findViewById(R.id.main_menu);
		main_menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					FlurryAgent.logEvent("Navigation_Menu");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				Intent intent=new Intent(context, ShortcutMenus.class);
				if(scrollPosition>=0)
				{
					intent.putExtra("finishAct", false);
				}
				startActivity(intent);
				overridePendingTransition(R.anim.activityfade_in, R.anim.activityfade_out);

			}
		});

		tapToEnter=(ImageView)findViewById(R.id.tapToEnter);
		tapToEnter.bringToFront();
		tapToEnter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,LatestTab.class);
				intent.putExtra("finishAct", false);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);


			}
		});
		if(tapCounter==1)
		{
			tapToEnter.setVisibility(View.INVISIBLE);
		}
		fbicon=(ImageView)findViewById(R.id.fbicon);
		twtricon=(ImageView)findViewById(R.id.twtricon);

		fbicon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try
				{
					FlurryAgent.logEvent("Madhuri_Facebook_Page");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				Intent intent=new Intent(context,FbTwtrLink.class);
				intent.putExtra("fb_link", 1);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		twtricon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					FlurryAgent.logEvent("Madhuri_Twitter_Page");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				Intent intent=new Intent(context,FbTwtrLink.class);
				intent.putExtra("fb_link", 2);
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});

		mainInfoText=(ImageView)findViewById(R.id.mainInfoText);

		if(isMenuTextShown==0)
		{
			mainInfoText.setVisibility(View.VISIBLE);
		}



		/*
		 * Getting Hight of Slide image to set it to Pager
		 */
		BitmapDrawable bd=(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.slider);
		sliderheight=bd.getIntrinsicHeight();

		/*
		 * Defining pager height and width
		 */
		params=new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,sliderheight);

		/*
		 * Setting Pager to the Bottom
		 */
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

		relPager.setLayoutParams(params);
		relPager.bringToFront();

		mainPager=(CustomViewPager)findViewById(R.id.mainPager);
		bottomPager=(CustomViewPager)findViewById(R.id.bottomPager);

		/*
		 * Adding Images to Slider
		 */
		name=new ArrayList<String>();
		btnName=new ArrayList<String>();
		mainScreenImag=new ArrayList<String>();
		animate=new ArrayList<String>();

		name.add("slider_with_text_off"); 
		name.add("slider1_transp");
		name.add("slider");//2	latest
		name.add("slider1_transp");
		name.add("slider");//4	dance
		name.add("slider1_transp");
		name.add("slider");//6	movies
		name.add("slider1_transp");
		name.add("slider");//8	fashion
		name.add("slider1_transp");
		name.add("slider");//10 health
		name.add("slider1_transp");
		name.add("slider");//12 art
		name.add("slider1_transp");
		name.add("slider");//14 gallery



		btnName.add("slider1_transp");
		btnName.add("slider1_transp");
		btnName.add("sliderbutton1"); //latest
		btnName.add("slider1_transp");
		btnName.add("sliderbutton2"); //dance
		btnName.add("slider1_transp");
		btnName.add("sliderbutton3"); //Movies
		btnName.add("slider1_transp");
		btnName.add("sliderbutton4"); //Fashion
		btnName.add("slider1_transp");
		btnName.add("sliderbutton5"); //Health
		btnName.add("slider1_transp");
		btnName.add("sliderbutton6");//Art
		btnName.add("slider1_transp");
		btnName.add("sliderbutton7");//gallery

		mainScreenImag.add("title_madhuri");
		mainScreenImag.add("sandvich0a");
		mainScreenImag.add("sandvich0b");

		/*mainScreenImag.add("latest_home");*/
		mainScreenImag.add("transparent");
		mainScreenImag.add("sandvich1a");
		mainScreenImag.add("sandvich1b");

		/*		mainScreenImag.add("dance_home");*/
		mainScreenImag.add("transparent");
		mainScreenImag.add("sandvich2a");
		mainScreenImag.add("sandvich2b");

		/*mainScreenImag.add("movies_home");*/
		mainScreenImag.add("transparent");
		mainScreenImag.add("sandvich3a");
		mainScreenImag.add("sandvich3b");

		/*		mainScreenImag.add("fashion_home");*/
		mainScreenImag.add("transparent");
		mainScreenImag.add("sandvich4a");
		mainScreenImag.add("sandvich4b");

		/*		mainScreenImag.add("health_home");*/
		mainScreenImag.add("transparent");
		mainScreenImag.add("sandvich5a");
		mainScreenImag.add("sandvich5b");

		/*	mainScreenImag.add("art_home");*/
		mainScreenImag.add("transparent");
		mainScreenImag.add("sandvich6a");
		mainScreenImag.add("sandvich6b");

		/*		mainScreenImag.add("gallery_home");*/
		mainScreenImag.add("transparent");



		animate.add("0");
		animate.add("0");
		animate.add("0");

		animate.add("1");
		animate.add("0");
		animate.add("0");

		animate.add("1");
		animate.add("0");
		animate.add("0");

		animate.add("1");
		animate.add("0");
		animate.add("0");

		animate.add("1");
		animate.add("0");
		animate.add("0");

		animate.add("1");
		animate.add("0");
		animate.add("0");

		animate.add("1");
		animate.add("0");
		animate.add("0");

		animate.add("1");



		BottomAdapter adpater=new BottomAdapter(getSupportFragmentManager(),context, name,btnName);

		madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);

		mainPager.setAdapter(madpater);

		mainPager.setPagingEnabled(false);




		bottomPager.setAdapter(adpater);


		bottomPager.setOnTouchListener(new BottomPagerTouchListener());

		/*
		 * Registering ViewPager for PageListener
		 */
		bottomPager.setOnPageChangeListener(new PageListener());



		/*
		 * Slowing Down the speed of pager
		 */
		try {
			Field mScroller;
			mScroller = ViewPager.class.getDeclaredField("mScroller");
			mScroller.setAccessible(true); 
			scroller = new FixedSpeedScroller(bottomPager.getContext(), new DecelerateInterpolator());
			// scroller.setFixedDuration(5000);

			mScroller.set(bottomPager, scroller);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}


		/*
		 * Slowing Down the speed of Main pager
		 */
		try {
			Field mScroller2;
			mScroller2 = ViewPager.class.getDeclaredField("mScroller");
			mScroller2.setAccessible(true); 
			/*MainFixedSpeedScroller scroller2 = new MainFixedSpeedScroller(mainPager.getContext(), new AccelerateDecelerateInterpolator());*/
			scroller2 = new MainFixedSpeedScroller(mainPager.getContext(),new AccelerateInterpolator(0.3f));
			// scroller.setFixedDuration(5000);
			mScroller2.set(mainPager, scroller2);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}

		/*mainPager.setPageMargin(-5);*/
		if(metrics.densityDpi==DisplayMetrics.DENSITY_LOW)
		{
			mainPager.setPageMargin(-14);
		}
		else
		{
			mainPager.setPageMargin(-5);
		}
		mainPager.setOnPageChangeListener(new MainScrollPageListener());
		mainPager.setOffscreenPageLimit(2);
	/*	mainPager.setOffscreenPageLimit(mainPager.getAdapter().getCount()-1);*/

		//	Log.i("","Main Pager : "+mainPager.getScrollX());

		/*	mainPager.setOffscreenPageLimit(3);
		bottomPager.setOffscreenPageLimit(3);*/

		/*bottomPager.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				fadeFlag=true;
			}
		});
		 */
	}
	
	

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "456H2RVTM3XG7H86MK95");
		
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);
		
	}
	



	/*
	 * bottomPager Touch Listener 
	 */
	private class BottomPagerTouchListener implements View.OnTouchListener
	{

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub

			if (gdt.onTouchEvent(event)) {
				return false;
			}



			return false;
		}

	}



	/*
	 * BottomPageChange
	 */
	private  class PageListener extends SimpleOnPageChangeListener{


		ImageView bottomImageView;


		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {
			// TODO Auto-generated method stub



		}


		public void onPageSelected(final int position) {

			try
			{
				try
				{
					FlurryAgent.logEvent("Navigation_Swipe");
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
				touch=false;
				isPageChanging=true;
				scrollPosition=position;
				bottomImageView=(ImageView)bottomPager.findViewWithTag(position);


				/*			StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {android.R.attr.state_pressed},
					getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName+"_selected", "drawable",context.getPackageName())));
			states.addState(new int[] {android.R.attr.state_focused},
					getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName+"_selected", "drawable",context.getPackageName())));
			states.addState(new int[] { },
					getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName, "drawable",context.getPackageName())));
				 */

				/*
				 * Changing Sliding image 
				 */
				if(scrollPosition==0 && bottomscrollPosition==0)
				{
					final StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {android.R.attr.state_pressed},
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_with_text_on", "drawable",context.getPackageName())));
					states.addState(new int[] {android.R.attr.state_focused},
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_with_text_on", "drawable",context.getPackageName())));
					states.addState(new int[] { },
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_with_text_off", "drawable",context.getPackageName())));
					bottomImageView.setClickable(true);
					bottomImageView.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							bottomImageView.setImageDrawable(states);
							return false;
						}
					});
				}
				else if(scrollPosition==0 && bottomscrollPosition>2)
				{
					bottomImageView.setImageResource(R.drawable.slider_without_text_off);
					final StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {android.R.attr.state_pressed},
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_without_text_selected", "drawable",context.getPackageName())));
					states.addState(new int[] {android.R.attr.state_focused},
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_without_text_selected", "drawable",context.getPackageName())));
					states.addState(new int[] { },
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_without_text_off", "drawable",context.getPackageName())));
					bottomImageView.setClickable(true);
					bottomImageView.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							bottomImageView.setImageDrawable(states);
							if (gdt.onTouchEvent(event)) {
								return false;
							}
							return false;
						}
					});
					/*SWIPE_MIN_DISTANCE=(scwidth/2)-30;*/
				}
				else
				{
					/*SWIPE_MIN_DISTANCE=(scwidth/2)-10;*/
					bottomImageView.setClickable(false);
				}






				/*scroller.setFixedDuration(2000);*/
				bottomscrollPosition++;
				//	Log.i("Counter: ",""+bottomscrollPosition);

				fadeFlag=false;

				count=false;

				if(scrollPosition!=0)
				{
					lastState=scrollPosition-1;




					/*		main_menu.setOnClickListener(null);
					fbicon.setOnClickListener(null);
					twtricon.setOnClickListener(null);*/
				}


				//	Log.i("==========="," mCurrentPage"+mCurrentPage+" CurrentPage "+bottomPager.getCurrentItem());
				/*			
				 * Fading out Last Image! 

			if((position==14) && (SCROLLED<0))
			{
				count=false;

			}
				 */

				if((position%2!=0 && position!=0)&&(SCROLLED>0))
				{
					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mCurrentPage=position+1;
							bottomPager.setCurrentItem(mCurrentPage, true);
							bottomPager.getAdapter().notifyDataSetChanged();
							if(bottomPager.getCurrentItem()>=14)
							{
								mCurrentPage=14;
								/*count=0;*/
							}

						}
					});


					switch(position)
					{

					case 1:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(3, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});

						break;
					}
					case 3:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(6, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 5:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(9, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 7:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(12, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 9:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*	mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(15, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 11:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(18, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 13:
					{
						/*
						 * Sliding Right MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(21, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}

					}

				}
				else if((position%2!=0 && position!=0)&&(SCROLLED<0))
				{
					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mCurrentPage=position-1;
							bottomPager.setCurrentItem(mCurrentPage, true);
							bottomPager.getAdapter().notifyDataSetChanged();
							if(bottomPager.getCurrentItem()==0)
							{
								mCurrentPage=0;
							}

						}
					});

					/*
					 * Sliding TO LEFT
					 */
					switch(position)
					{

					case 1:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(0, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});

						break;
					}
					case 3:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(3, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 5:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(6, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 7:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(9, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 9:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*	mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(12, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 11:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(15, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();
								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});
						break;
					}
					case 13:
					{
						/*
						 * Sliding LEFT MainPager
						 */
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								/*mMainCurrentPage=mainPager.getCurrentItem()+3;*/
								mainPager.setCurrentItem(18, true);
								mainPager.getAdapter().notifyDataSetChanged();
								mMainCurrentPage=mainPager.getCurrentItem();

								/*if(mainPager.getCurrentItem()>=12)
							{
								mCurrentPage=12;
							}*/

							}
						});

						break;
					}

					}

				}

			}catch(Exception ex)
			{
				ex.printStackTrace();
			}

		}


		@Override
		public void onPageScrollStateChanged(int state) {
			// TODO Auto-generated method stub

			/*
			 * Idle State
			 */
			if(state==0)
			{

				/*if(!tapToEnter.isShown())
				{
					if((mainScreenPage==3)&&(tapCounter==1))
					{
						tapToEnter.setVisibility(View.VISIBLE);
						tapCounter=0;
					}
				}*/
				/*	 * Fadding in icons*/

				/*if((icFade) &&(!main_menu.isShown() && !twtricon.isShown() && !fbicon.isShown()))
				{
					Animation animation=AnimationUtils.loadAnimation(context, R.anim.iconfade_in);
					fbicon.startAnimation(animation);
					twtricon.startAnimation(animation);
					main_menu.startAnimation(animation);
					fbicon.setVisibility(View.VISIBLE);
					twtricon.setVisibility(View.VISIBLE);
					main_menu.setVisibility(View.VISIBLE);
				}

				 */
				main_menu.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(context, ShortcutMenus.class);
						if(scrollPosition>=0)
						{
							intent.putExtra("finishAct", false);
						}
						startActivity(intent);
						overridePendingTransition(R.anim.activityfade_in, R.anim.activityfade_out);

					}
				});



				fbicon.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub


						Intent intent=new Intent(context,FbTwtrLink.class);
						intent.putExtra("fb_link", 1);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}
				});

				twtricon.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub


						Intent intent=new Intent(context,FbTwtrLink.class);
						intent.putExtra("fb_link", 2);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}
				});




				isPageChanging=true;

				scroller.setFixedDuration(2500);
				//	Log.i("", "MisPage Changing----"+isPageChanging);
				//	Log.i("-----", "MainPager "+mainPager.getCurrentItem()+" Scrolled "+mainPager.getScrollX());

				//	Log.i("","Triggered");
				if(TOAST)
				{
					Toast.makeText(context, "Distance  x "+DISTANCEX, Toast.LENGTH_SHORT).show();
				}
				bottomPager.setPagingEnabled(true);

				switch (scrollPosition) {
				case 0:
				{
					constantscroll=0;
					break;
				}
				case 2:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//Log.i("","M Changed Val "+constantscroll);
					break;
				}
				case 4:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//	Log.i("","M Changed Val "+constantscroll);
					break;
				}
				case 6:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//		Log.i("","M Changed Val "+constantscroll);
					break;
				}
				case 8:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//		Log.i("","M Changed Val "+constantscroll);
					break;
				}
				case 10:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//			Log.i("","M Changed Val "+constantscroll);
					break;
				}
				case 12:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//		Log.i("","M Changed Val "+constantscroll);
					break;
				}
				case 14:
				{
					constantscroll=(mainPager.getCurrentItem()+1)*scwidth;
					//			Log.i("","M Changed Val "+constantscroll);
					break;
				}

				default:
					break;
				}

				/*if(scrollPosition==0)
				{
					final StateListDrawable states = new StateListDrawable();
					states.addState(new int[] {android.R.attr.state_pressed},
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_with_text_selected", "drawable",context.getPackageName())));
					states.addState(new int[] {android.R.attr.state_focused},
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_with_text_selected", "drawable",context.getPackageName())));
					states.addState(new int[] { },
							getResources().getDrawable(context.getResources().getIdentifier("drawable/slider_with_text_off", "drawable",context.getPackageName())));
					bottomImageView.setClickable(true);
					bottomImageView.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							bottomImageView.setImageDrawable(states);
							return false;
						}
					});
				}
				else
				{
					bottomImageView.setClickable(false);
				}
				 */
			}


			/*
			 * Dragging
			 */
			if(state==1)
			{
				/*
				 * Disabling click
				 */

				/*if(tapToEnter.isShown())
				{
					Animation animationt=AnimationUtils.loadAnimation(context, R.anim.fade_out);
					tapToEnter.startAnimation(animationt);
					tapToEnter.setVisibility(View.INVISIBLE);
				}
*/
				imgBack.setOnClickListener(null);
				main_menu.setOnClickListener(null);
				fbicon.setOnClickListener(null);
				twtricon.setOnClickListener(null);


				/*Animation animation=AnimationUtils.loadAnimation(context, R.anim.fade_out);
				fbicon.startAnimation(animation);
				twtricon.startAnimation(animation);
				main_menu.startAnimation(animation);
				fbicon.setVisibility(View.INVISIBLE);
				twtricon.setVisibility(View.INVISIBLE);
				main_menu.setVisibility(View.INVISIBLE);*/


				//	Log.i("", "MisPage Changing----"+isPageChanging);
				try
				{

					scroller.setFixedDuration(2500);
					if(TOAST)
					{
						Toast.makeText(context, "you Clicked", Toast.LENGTH_SHORT).show();
					}

				}catch(Exception ex)
				{
					ex.printStackTrace();	
				}

				/*new Handler().post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						if(!mainPager.isFakeDragging())
						{
							mainPager.beginFakeDrag();
							mainPager.fakeDragBy(-(SCROLLED));
							mainPager.endFakeDrag();

						}
					}
				});
				 */

			}

			/*
			 * Setteling
			 */
			if(state==2)
			{

				bottomPager.setPagingEnabled(false);


				/*
				 * RND
				 */
				//	Log.i("", "MisPage Changing----"+isPageChanging);
				if(!isPageChanging)
				{
					//		Log.i("", "Ma Scroll Setteling : "+mainPager.getCurrentItem()+" bottom "+(constantscroll-constantscrolled));

					/*if(SCROLLED>0)
					{
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.fakeDragBy(-(constantscrolled-constantscroll));
									mainPager.scrollBy(-(constantscrolled-constantscroll), 0);
									mainPager.endFakeDrag();
								}
							}
						});
					}else if(SCROLLED<0)
					{
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.fakeDragBy((constantscrolled-constantscroll));
									mainPager.scrollBy((constantscrolled-constantscroll), 0);
									mainPager.endFakeDrag();
								}
							}
						});
					}*/
					/*new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(mainScreenPage);
									mainPager.getAdapter().notifyDataSetChanged();
									mainPager.endFakeDrag();
								}
							}
						});*/

					//	Log.i("", "MPage S: "+scrollPosition);


					/*switch (scrollPosition) {
					case 0:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(0,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(0);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater.changePage(0);
							}
						});
						break;

					}
					case 2:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(3,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(3);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(3);
							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					case 4:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
										mainPager.beginFakeDrag();
									mainPager.setCurrentItem(6,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(6);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater.changePage(6);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(6);

							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					case 6:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(9,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(9);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(9);

							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					case 8:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(12,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(12);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(12);
							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					case 10:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(15,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(15);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(15);
							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					case 12:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(18,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(18);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(18);

							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					case 14:
					{

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								if(!mainPager.isFakeDragging())
								{
									mainPager.beginFakeDrag();
									mainPager.setCurrentItem(21,true);
									mainPager.getAdapter().notifyDataSetChanged();
									madpater.changePage(21);
									mainPager.endFakeDrag();
								}
								Log.i("", "MPage: "+scrollPosition);
								madpater=null;
								madpater=new MainAdapter(getSupportFragmentManager(), context, mainScreenImag, animate);
								mainPager.setAdapter(madpater);
								mainPager.setCurrentItem(21);

							}
						});
						mainPager.getAdapter().notifyDataSetChanged();
						break;
					}
					default:
						break;
					}*/

					//mainPager.getAdapter().notifyDataSetChanged();

					//	scroller.setFixedDuration(1350);
				}
				/*if(count)
				{
				scroller.setFixedDuration(500);
				}*/


				/*	scroller.setFixedDuration(1350);*/
				if(fadeFlag)
				{
					switch(mainScreenPage)
					{

					case 3: //latest
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}

						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.latest_home, options);

						/*	imgBack.setImageResource(R.drawable.latest_home);*/
						imgBack.setImageBitmap(bm);
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);
						break;
					}
					case 6: //dance
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}

						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.dance_home, options);
						imgBack.setImageBitmap(bm);


						/*	imgBack.setImageResource(R.drawable.dance_home);*/
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);

						break;
					}
					case 9: //movies
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}

						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.movies_home, options);
						imgBack.setImageBitmap(bm);

						/*imgBack.setImageResource(R.drawable.movies_home);*/
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);

						break;
					}
					case 12://fashion
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}


						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.fashion_home, options);
						imgBack.setImageBitmap(bm);

						/*	imgBack.setImageResource(R.drawable.fashion_home);*/
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);

						break;
					} 
					case 15: //health
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}


						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.health_home, options);
						imgBack.setImageBitmap(bm);

						/*imgBack.setImageResource(R.drawable.health_home);*/
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);

						break;
					}
					case 18: //art
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}

						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.art_home, options);
						imgBack.setImageBitmap(bm);

						/*imgBack.setImageResource(R.drawable.art_home);*/
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);

						break;
					}
					case 21: //gallery
					{
						if(TOAST)
						{
							Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
						}

						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig =Config.ARGB_8888;
						options.inScaled = false;
						options.inPurgeable=true;
						Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.gallery_home, options);
						imgBack.setImageBitmap(bm);

						/*imgBack.setImageResource(R.drawable.gallery_home);*/
						anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
						imgBack.startAnimation(anim);

						break;
					}


					}
				}


			}


		}


	}


	private class MainScrollPageListener extends SimpleOnPageChangeListener{

		int tapPos;


		@Override
		public void onPageSelected(int position) {
			// TODO Auto-generated method stub


			tapPos=position;
			Log.i("", "MChangingWhile******");

			isPageChanging=true;

			mainScreenPage=position;
			
			
			
			if(position!=0)
			{
				mainInfoText.setVisibility(View.INVISIBLE);

			}
			switch(mainScreenPage)
			{
			case 3: //latest
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.latest_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.latest_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			}
			case 6: //dance
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.dance_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.dance_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			}
			case 9: //movies
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}



				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.movies_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.movies_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			}
			case 12://fashion
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.fashion_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.fashion_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			} 
			case 15: //health
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.health_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.health_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			}
			case 18: //art
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.art_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.art_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			}
			case 21: //gallery
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.ARGB_8888;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.gallery_home, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);

				/*imgBack.setImageResource(R.drawable.gallery_home);*/
				anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
				imgBack.startAnimation(anim);
				break;
			}


			}
		}



		/*
		 * (non-Javadoc)
		 * @see android.support.v4.view.ViewPager.SimpleOnPageChangeListener#onPageScrollStateChanged(int)
		 * Main Page State
		 */
		@Override
		public void onPageScrollStateChanged(int state) {
			// TODO Auto-generated method stub

			if(state==0)
			{

				/*Intent intent=new Intent();
				intent.setAction(getPackageName()+getLocalClassName());
				intent.putExtra("settled","yes");
				sendBroadcast(intent);*/

				//Toast.makeText(context, "BroadCast", Toast.LENGTH_SHORT).show();

				icFade=true;
				/*	if((icFade) &&(!main_menu.isShown() && !twtricon.isShown() && !fbicon.isShown()))
				{
					Animation animation=AnimationUtils.loadAnimation(context, R.anim.iconfade_in);
					fbicon.startAnimation(animation);
					twtricon.startAnimation(animation);
					main_menu.startAnimation(animation);
					fbicon.setVisibility(View.VISIBLE);
					twtricon.setVisibility(View.VISIBLE);
					main_menu.setVisibility(View.VISIBLE);
				}*/

				Animation animation=AnimationUtils.loadAnimation(context, R.anim.iconfade_in);
				fbicon.startAnimation(animation);
				twtricon.startAnimation(animation);
				main_menu.startAnimation(animation);
				fbicon.setVisibility(View.VISIBLE);
				twtricon.setVisibility(View.VISIBLE);
				main_menu.setVisibility(View.VISIBLE);


				/*
				 * Tap to Enter on Main Image
				 */

				switch(mainScreenPage)
				{
				case 0:
				{
					imgBack.setOnClickListener(null);
					break;
				}
				case 3: //latest
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,LatestTab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

						}
					});

					constantscroll=scwidth*(mainScreenPage+1);
					//		Log.i("", "MainSc: "+constantscroll);

					break;
				}
				case 6: //dance
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,DanceTab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});

					constantscroll=scwidth*(mainScreenPage+1);
					//			Log.i("", "MainSc: "+constantscroll);


					break;
				}
				case 9: //movies
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,MoviesTab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});
					constantscroll=scwidth*(mainScreenPage+1);
					//		Log.i("", "MainSc: "+constantscroll);

					break;
				}
				case 12://fashion
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,FashionTab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});
					constantscroll=scwidth*(mainScreenPage+1);
					//			Log.i("", "MainSc: "+constantscroll);

					break;
				} 
				case 15: //health
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,HealthTab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});
					constantscroll=scwidth*(mainScreenPage+1);
					//		Log.i("", "MainSc: "+constantscroll);

					break;
				}
				case 18: //art
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,ArtTab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});
					constantscroll=scwidth*(mainScreenPage+1);
					//		Log.i("", "MainSc: "+constantscroll);

					break;
				}
				case 21: //gallery
				{
					imgBack.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							tapSound.start();
							Intent intent=new Intent(context,Gallery_Tab.class);
							intent.putExtra("finishAct", false);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					});
					constantscroll=scwidth*(mainScreenPage+1);
					//	Log.i("", "MainSc: "+constantscroll);

					break;
				}

				}


				if((tapPos==3)&&(tapCounter==1))
				{
					tapToEnter.setVisibility(View.VISIBLE);
					tapCounter=0;
				}
				else
				{
					/*tapToEnter.setVisibility(View.INVISIBLE);*/

					tapToEnter.setVisibility(View.INVISIBLE);
				}

				switch (tapPos)
				{
				case 3:
				{
					count=true;
					break;
				}
				case 6:
				{
					count=true;
					break;
				}
				case 9:
				{
					count=true;
					break;
				}
				case 12:
				{
					count=true;
					break;
				}
				case 15:
				{
					count=true;
					break;
				}
				case 18:
				{
					count=true;
					break;
				}
				case 21:
				{
					count=true;
					break;
				}
				default :
					count=false;
				}

			}
			if(state!=0)
			{
				icFade=false;

				tapToEnter.setVisibility(View.INVISIBLE);
				scroller.setFixedDuration(2000);
				fbicon.setVisibility(View.INVISIBLE);
				twtricon.setVisibility(View.INVISIBLE);
				main_menu.setVisibility(View.INVISIBLE);


				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);

				/*	imgBack.setImageResource(R.drawable.transparent);*/
				count=false;

				imgBack.setOnClickListener(null);

			}


		}

		@Override
		public void onPageScrolled(int position, float positionOffset,
				int positionOffsetPixels) {


			// TODO Auto-generated method stub
			/*			mainPager.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					return true;
				}
			});*/

			/*Intent intent=new Intent();
			intent.setAction(getPackageName()+getLocalClassName());
			intent.putExtra("settled","no");
			sendBroadcast(intent);*/
			//	Toast.makeText(context, "BroadCast", Toast.LENGTH_SHORT).show();
		}

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_madhuri_home_screen, menu);
		return false;
	}



	public class BottomAdapter extends FragmentStatePagerAdapter
	{

		Context context;
		ArrayList<String>imgname=null;
		ArrayList<String>btnName;
		public BottomAdapter(FragmentManager fm,Context context,ArrayList<String>imgname,ArrayList<String>btnName) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.imgname=imgname;
			this.btnName=btnName;
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new BottomImageFragment(context, imgname.get(position),btnName.get(position),position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imgname.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub
			/*		FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();*/
			super.destroyItem(container, position, object);
		}


	}


	public  class MainAdapter extends FragmentStatePagerAdapter
	{

		Context context;
		ArrayList<String>imgname=null;
		ArrayList<String>animate;
		public MainAdapter(FragmentManager fm,Context context,ArrayList<String>imgname,ArrayList<String>animate) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.imgname=imgname;
			this.animate=animate;
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			//		Log.i("", "M here too"+position);
			return new MainImageFragment(context, imgname.get(position),animate.get(position),position);
			/*return MainImageFragment.newInstance(context, imgname.get(position),animate.get(position),position);*/

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imgname.size();
		}

		public  void changePage(int page)
		{
			getItem(page);
			notifyDataSetChanged();
			//		Log.i("", "M here"+page);
		}



		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub
			/*FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.disallowAddToBackStack();
			trans.remove((Fragment) object);
			trans.commit();*/
			super.destroyItem(container, position, object);
		}


	}


	private class GestureListener extends SimpleOnGestureListener
	{


		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			// TODO Auto-generated method stub
			if(TOAST)
			{
				Toast.makeText(context, "Flinged"+Math.abs(velocityX), Toast.LENGTH_SHORT).show();
			}
			mSelectedCurrentPage=bottomPager.getCurrentItem();
			mainSelectedPage=mainPager.getCurrentItem();

			if((e1.getX() - e2.getX()) >= SWIPE_MIN_DISTANCE ) {

				try
				{
					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mCurrentPage+=2;
							bottomPager.setCurrentItem(mCurrentPage, true);
							bottomPager.getAdapter().notifyDataSetChanged();
							if(bottomPager.getCurrentItem()>=14)
							{
								mCurrentPage=14;
								/*				count=0;*/
							}

						}
					});

					/*
					 * Sliding Right MainPager
					 */
					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mMainCurrentPage+=3;
							mainPager.setCurrentItem(mMainCurrentPage, true);
							mainPager.getAdapter().notifyDataSetChanged();
							/*if(mainPager.getCurrentItem()>=12)
						{
							mCurrentPage=12;
						}*/

						}
					});

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}



				return false; // Right to left
			}  

			else if ((e2.getX() - e1.getX()) > SWIPE_MIN_DISTANCE) {

				try
				{
					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mCurrentPage-=2;
							bottomPager.setCurrentItem(mCurrentPage, true);
							bottomPager.getAdapter().notifyDataSetChanged();
							if(bottomPager.getCurrentItem()==0)
							{
								mCurrentPage=0;
							}

						}
					});

					/*
					 * Sliding Left MainPager
					 */
					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							mMainCurrentPage-=3;
							mainPager.setCurrentItem(mMainCurrentPage, true);
							mainPager.getAdapter().notifyDataSetChanged();
							/*	if(mainPager.getCurrentItem()>=12)
						{
							mCurrentPage=12;
						}*/

						}
					});

				}catch(Exception ex)
				{
					ex.printStackTrace();
				}



				return false; // Left to right
			}
			else
			{
				try
				{

					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							//mCurrentPage-=2;
							bottomPager.setCurrentItem(mSelectedCurrentPage, true);
							bottomPager.getAdapter().notifyDataSetChanged();

							if(bottomPager.getCurrentItem()==0)
							{
								mCurrentPage=0;
							}

						}
					});
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}



			}

			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			// TODO Auto-generated method stub

			isPageChanging=false;
			SCROLLED=(int) (e1.getX()-e2.getX());
			int distancex=0;
			//		Log.i("---------","e1.getX()"+e1.getX()+" e2.getX() "+e2.getX()+" getRawX()"+e1.getRawX()+" e2.getRawX() "+e2.getRawX());

			scrolledValue=(int) (e1.getX()-e2.getX());

			DISTANCEX=distanceX;

			/*if(SCROLLED>0 && scrollPosition!=14)
			{
				mainPager.scrollBy((int) (distanceX*2), 0);
				if(!mainPager.isFakeDragging())
				{
					mainPager.beginFakeDrag();
					mainPager.fakeDragBy(-bottomPager.getScrollX());
					mainPager.endFakeDrag();
				}
			}
			else if(SCROLLED<0 && scrollPosition!=0)
			{
				mainPager.scrollBy((int) (distanceX*2), 0);
				mainPager.beginFakeDrag();
				mainPager.fakeDragBy(distanceX*2);
				mainPager.endFakeDrag();
			}*/


			/*mainPager.scrollBy((int) (DISTANCEX*2), 0);*/
			//constantscroll=mainPager.getScrollX();

			constantscrolled=mainPager.getScrollX();
			//	Log.i("", "Constant Scrolled "+constantscrolled+" MainPager : "+mainPager.getScrollX()+" MainPage : "+mainScreenPage);


			/*mainPager.scrollBy((int)distanceX*2, 0);
			mainPager.scrollBy(-(int)distanceX*2, 0);*/
			/*			mainPager.scrollBy(8, 0);
			mainPager.computeScroll();*/
			/*		mainPager.scrollBy((int)distanceX*4, 0);*/
			/*		scroller2.startScroll(1, 0, 10, 0);*/

			/*if(scrollPosition!=0 && scrollPosition!=14)
			{
			mainPager.scrollBy((int) (distanceX*4), 0);
			DISTANCEX=(int)(distanceX*4);
			}*/

			/*if(SCROLLED>0 && scrollPosition!=14)
			{
				mainPager.scrollBy(8, 0);
			}
			else if(SCROLLED<0 && scrollPosition!=0)
			{
				mainPager.scrollBy(-8, 0);
			}*/
			/*	mainPager.scrollBy((int) distanceX*2, 0);*/
			//Log.i("********","SCROLLED "+SCROLLED);

			/*			mainPager.scrollTo(SCROLLED*2, 0);*/


			switch(mainScreenPage)
			{

			case 3: //latest
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);

				/*	imgBack.setImageResource(R.drawable.transparent);*/
				imgBack.startAnimation(anim);

				fadeFlag=true;
				scrolledValue=0;

				break;
			}
			case 6: //dance
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*			imgBack.setImageResource(R.drawable.transparent);*/
				imgBack.startAnimation(anim);
				fadeFlag=true;
				scrolledValue=0;
				break;
			}
			case 9: //movies
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);

				/*imgBack.setImageResource(R.drawable.transparent);*/
				imgBack.startAnimation(anim);
				fadeFlag=true;
				scrolledValue=0;
				break;
			}
			case 12://fashion
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);


				/*imgBack.setImageResource(R.drawable.transparent);*/
				imgBack.startAnimation(anim);
				fadeFlag=true;
				scrolledValue=0;
				break;
			} 
			case 15: //health
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);



				/*imgBack.setImageResource(R.drawable.transparent);*/
				imgBack.startAnimation(anim);
				fadeFlag=true;
				scrolledValue=0;
				break;
			}
			case 18: //art
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}


				anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig =Config.RGB_565;
				options.inScaled = false;
				options.inPurgeable=true;
				Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

				/*	imgBack.setImageResource(R.drawable.latest_home);*/
				imgBack.setImageBitmap(bm);

				/*imgBack.setImageResource(R.drawable.transparent);*/
				imgBack.startAnimation(anim);
				fadeFlag=true;
				scrolledValue=0;
				break;
			}
			case 21: //gallery
			{
				if(TOAST)
				{
					Toast.makeText(context, "Fadeout at "+scrollPosition, Toast.LENGTH_SHORT).show();
				}

				if(bottomPager.getCurrentItem()==14 && SCROLLED >0)
				{

				}
				else
				{
					anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);

					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inPreferredConfig =Config.RGB_565;
					options.inScaled = false;
					options.inPurgeable=true;
					Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.transparent, options);

					/*	imgBack.setImageResource(R.drawable.latest_home);*/
					imgBack.setImageBitmap(bm);
					/*imgBack.setImageResource(R.drawable.transparent);*/
					imgBack.startAnimation(anim);
					fadeFlag=true;
					scrolledValue=0;
				}
				break;
			}



			}


			return super.onScroll(e1, e2, distanceX, distanceY);
			/*return false;*/
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			return super.onSingleTapUp(e);

		}

	}//end of Gesture Listener


	


	/*
	 * BroadCastReciver
	 */

	/*class BroadCast extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String res =intent.getStringExtra("settled");
			if(res.equalsIgnoreCase("yes"))
			{
				count=true;
			}
			if(res.equalsIgnoreCase("no"))
			{
				count=false;
			}
		}

	}
	 */


}

