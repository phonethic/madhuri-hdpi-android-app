package com.phonethics.madhuridixitnene;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectSerializer {

	/**
	 * Serializes the object to a file
	 * 
	 * @param objToSerialize
	 */

	public void serialize(Object objToSerialize,String fileName) {

		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
			out.writeObject(objToSerialize);
			out.close();
			System.out.println("Object : " + objToSerialize.getClass()+ " serialized successfully");
		} catch (Exception ex) {
			System.out.println("Error Saving Object to File :"+ ex.getMessage());
			ex.printStackTrace();
		}
	}


	/**
	 * De-serialize the Object from the file
	 */
	public Object deserialize(String fileName) {

		Object deserializedObject = null;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
			deserializedObject = in.readObject();
			in.close();
			System.out.println("Object : " + deserializedObject.getClass()+ " de-serialized successfully");
		} catch (Exception ex) {
			System.out.println("Error Reading Object to File :" + ex.getMessage());
			ex.printStackTrace();

		}

		return deserializedObject;

	}
}
