package com.phonethics.madhuridixitnene;


import java.io.File;
import java.lang.ref.WeakReference;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.ImageDownloader;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;


import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;


public class MainImageFragment extends Fragment{

	Context context;
	String imgname;
	String animate;
	int tagPosition;
	Bitmap bm;

	com.nostra13.universalimageloader.core.ImageLoader imageLoader;
	File cacheDir;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;


	ImageView imgView;
	private int scwidth;
	private int scheight;

	private static final String DISK_CACHE_SUBDIR = "MMM";
	private final Object mDiskCacheLock = new Object();
	private DiskLruCache mDiskLruCache;
	private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
	private boolean mDiskCacheStarting = true;


	private LruCache<String, Bitmap> mMemoryCache;
	public MainImageFragment()
	{

	}
	public MainImageFragment(Context context,String imgname,String animate,int position)
	{
		this.context=context;
		this.imgname=imgname;
		this.animate=animate;
		tagPosition=position;

	}
	/*
	static MainImageFragment newInstance(Context contextx,String imgName,String Animate,int position1)
	{
		MainImageFragment f=new MainImageFragment();
		context=contextx;
		imgname=imgName;
		animate=Animate;
		tagPosition=position1;
		return f;
	}
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);


		imageLoader =ImageLoader.getInstance();
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MDMAIN");
		else
			cacheDir=context.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();


		/*config= new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.discCache(new UnlimitedDiscCache(cacheDir))
		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))
		.enableLogging()
		.build();
		imageLoader.init(config);*/
		/*config = new ImageLoaderConfiguration.Builder(context)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.discCache(new UnlimitedDiscCache(cacheDir))
		.denyCacheImageMultipleSizesInMemory()
		.memoryCacheSize(2*1024*1024)
		.memoryCache(new WeakMemoryCache())
		.imageDownloader(new ExtendedImageDownloader(context))
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.enableLogging() // Not necessary in common
		.build();
		imageLoader.init(config);


		options = new DisplayImageOptions.Builder()

		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
		 */



		/*
		 * Disk Cache
		 */
		/*File cacheDir = DiskLruCache.getDiskCacheDir(context, DISK_CACHE_SUBDIR);
		new InitDiskCacheTask().execute(cacheDir);
		 */

		// Get memory class of this device, exceeding this amount will throw an
		// OutOfMemory exception.

		/*
		 * Memory Cache
		 */

		try
		{
			final int memClass = ((ActivityManager) getActivity().getSystemService(
					getActivity().ACTIVITY_SERVICE)).getMemoryClass();

			// Use 1/8th of the available memory for this memory cache.

			Log.i("","Memcache"+memClass);
			final int cacheSize = 1024 * 1024 * memClass/8;

			/*RetainFragment mRetainFragment =
				RetainFragment.findOrCreateRetainFragment(getFragmentManager());*/

			/*if (mMemoryCache == null) {
			mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
				@SuppressLint("NewApi")
				@Override
				protected int sizeOf(String key, Bitmap bitmap) {
					// The cache size will be measured in bytes rather than number of items.
									    return bitmap.getByteCount();
					int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					if(currentapiVersion>android.os.Build.VERSION_CODES.HONEYCOMB) {
						return bitmap.getByteCount();
					} else {
						return bitmap.getRowBytes() * bitmap.getHeight();
					}
				}
			};

				 // Initialize cache here as usual
			}*/

			mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
				@SuppressLint("NewApi")
				@Override
				protected int sizeOf(String key, Bitmap bitmap) {
					// The cache size will be measured in bytes rather than number of items.
					/*  return bitmap.getByteCount();*/
					int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					if(currentapiVersion>android.os.Build.VERSION_CODES.HONEYCOMB) {
						return bitmap.getByteCount();
					} else {
						return bitmap.getRowBytes() * bitmap.getHeight();
					}
				}
			};
		}
		catch(NullPointerException npe)
		{
			npe.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();

		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub

		super.onStop();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		if(savedInstanceState!=null)
		{
			imgname=savedInstanceState.getString("imgName");
			animate=savedInstanceState.getString("animate");
			context=getActivity().getApplicationContext();
			scwidth=savedInstanceState.getInt("scwidth");
			scheight=savedInstanceState.getInt("scheight");
		}

		View view=(View)inflater.inflate(R.layout.mainimagelayout,null);
		try
		{
			imgView=(ImageView)view.findViewById(R.id.mainImage);
			imgView.setTag(tagPosition);
			BitmapDrawable bd=(BitmapDrawable) getActivity().getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()));
			scwidth=bd.getIntrinsicWidth();
			scheight=bd.getIntrinsicHeight();

			/*	imageLoader.displayImage(context.getResources().get(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())), imgView, options);*/


			/*imageLoader.displayImage("drawable://"+context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()), imgView, options);*/


			/*imageLoader.displayImage("drawable://"+context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()), imgView, options);*/




			/*imgView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					switch(tagPosition)
					{
					case 3: //latest
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),LatestTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);

						break;
					}
					case 6: //dance
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),DanceTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						break;
					}
					case 9: //movies
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),MoviesTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						break;
					}
					case 12://fashion
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),FashionTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						break;
					} 
					case 15: //health
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),HealthTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						break;
					}
					case 18: //art
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),ArtTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						break;
					}
					case 21: //gallery
					{
						Intent intent=new Intent(getActivity().getApplicationContext(),Gallery_Tab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						break;
					}

					}

				}
			});*/

			/*bm=bitmap(imgname);
			imgView.setImageBitmap(bm);*/
			/*	imageloader.DisplayImage(imgname, imgView);*/
			//imgView.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())));


			/*imgView.setImageBitmap(
					decodeSampledBitmapFromResource(context.getResources(),context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()), 800, 480));*/



			if(animate.equalsIgnoreCase("1"))
			{
				/*Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_in);
			anim.setFillAfter(true);
			imgView.startAnimation(anim);*/
				imgView.setVisibility(View.INVISIBLE);
			}

		}catch(OutOfMemoryError oom)
		{
			oom.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}



		return view;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if(savedInstanceState!=null)
		{
			imgname=savedInstanceState.getString("imgName");
			animate=savedInstanceState.getString("animate");
			scwidth=savedInstanceState.getInt("scwidth");
			scheight=savedInstanceState.getInt("scheight");
		}
		int resId=context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName());
		try
		{
			loadBitmap(resId, imgView);
		}catch(OutOfMemoryError oom)
		{
			oom.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}

	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("imgName", imgname);
		outState.putString("animate", animate);
		outState.putInt("scwidth", scwidth);
		outState.putInt("scheight", scheight);
		context=getActivity().getApplicationContext();

	}

	/*private Bitmap bitmap(String name)
	{
		final Bitmap bitmap=BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(name, "drawable", getActivity().getPackageName()));
		return bitmap;
	}*/

	/*
	 * Disk cache
	 */

	class InitDiskCacheTask extends AsyncTask<File, Void, Void> {
		@Override
		protected Void doInBackground(File... params) {
			synchronized (mDiskCacheLock) {
				File cacheDir = params[0];
				mDiskLruCache = DiskLruCache.openCache(context,cacheDir, DISK_CACHE_SIZE);
				mDiskCacheStarting = false; // Finished initialization
				mDiskCacheLock.notifyAll(); // Wake any waiting threads
			}
			return null;
		}
	}


	/*	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		FragmentManager manager = getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();
		trans.disallowAddToBackStack();
		trans.remove(this);
		trans.commit();
		super.onDetach();
	}*/
	public void loadBitmap(int resId, ImageView mImageView) {
		/*mImageView.setImageResource(R.drawable.image_placeholder);*/
		/*BitmapWorkerTask task = new BitmapWorkerTask(mImageView);
		task.execute(resId);
		 */



		/*
		 * Memeroy cache
		 */
		/*final String imageKey = String.valueOf(resId);

		final Bitmap bitmap = getBitmapFromMemCache(imageKey);
		if (bitmap != null) {
			mImageView.setImageBitmap(bitmap);
		} else {
			mImageView.setImageResource(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()));
			BitmapWorkerTask task = new BitmapWorkerTask(mImageView);
			task.execute(resId);
		}*/
		final String imageKey = String.valueOf(resId);
		Bitmap mPlaceHolderBitmap = null;
		if(mMemoryCache!=null)
		{
			mPlaceHolderBitmap=getBitmapFromMemCache(imageKey);
		}
		/*	else if(mMemoryCache==null)
		{
			mPlaceHolderBitmap=BitmapFactory.decodeResource(getResources(),  context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()));
			Log.i("", "Loaded From Local"+tagPosition);
			addBitmapToCache(imageKey, mPlaceHolderBitmap);
		}*/
		if(mPlaceHolderBitmap!=null)
		{
			mImageView.setImageBitmap(mPlaceHolderBitmap);
		}
		else if (cancelPotentialWork(resId, mImageView)) {
			final BitmapWorkerTask task = new BitmapWorkerTask(mImageView);
			final AsyncDrawable asyncDrawable =
					new AsyncDrawable(getResources(),mPlaceHolderBitmap, task);
			mImageView.setImageDrawable(asyncDrawable);
			task.execute(resId);
			Log.i("", "Loaded From Memory"+tagPosition);
		}
		if(mPlaceHolderBitmap==null)
		{
			Log.i("", "Loaded from Local"+tagPosition);
			
			mPlaceHolderBitmap = decodeSampledBitmapFromResource(
					getResources(), resId, scwidth, scheight);
			addBitmapToMemoryCache(imageKey, mPlaceHolderBitmap);
			imgView.setImageBitmap(mPlaceHolderBitmap);
		}


	}



	class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
		private final WeakReference<ImageView> imageViewReference;
		private int data = 0;

		public BitmapWorkerTask(ImageView imageView) {
			// Use a WeakReference to ensure the ImageView can be garbage collected
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		// Decode image in background.
		@Override
		protected Bitmap doInBackground(Integer... params) {
			final String imageKey = String.valueOf(params[0]);

			/*      // Check disk cache in background thread
		        Bitmap bitmap = getBitmapFromDiskCache(imageKey);

		        data = params[0];

		        if (bitmap == null) { // Not found in disk cache
		            // Process as normal
		           bitmap = decodeSampledBitmapFromResource(getResources(), data, scwidth, scheight);
		        }*/

			//  Add final bitmap to caches
			// addBitmapToCache(imageKey, bitmap);

			Bitmap bitmap = null;
			if(isAdded())
			{
				bitmap = decodeSampledBitmapFromResource(
						getResources(), params[0], scwidth, scheight);
				addBitmapToMemoryCache(String.valueOf(params[0]), bitmap);
			}

			/*	Bitmap bitmap = null;
			if (isCancelled()) {
	            bitmap = null;
	        }

	        if (imageViewReference != null && bitmap != null) {
	            final ImageView imageView = imageViewReference.get();
	            final BitmapWorkerTask bitmapWorkerTask =
	                    getBitmapWorkerTask(imageView);
	            if (this == bitmapWorkerTask && imageView != null) {
	                imageView.setImageBitmap(bitmap);
	            	addBitmapToMemoryCache(String.valueOf(params[0]), bitmap);

	            }
	        }*/

			return bitmap;



		}

		// Once complete, see if ImageView is still around and set bitmap.
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			/*if (imageViewReference != null && bitmap != null) {
				final ImageView imageView = imageViewReference.get();
				if (imageView != null) {
					imageView.setImageBitmap(bitmap);
				}
			}*/


			if (isCancelled()) {
				bitmap = null;
			}

			if (imageViewReference != null && bitmap != null) {
				final ImageView imageView = imageViewReference.get();
				final BitmapWorkerTask bitmapWorkerTask =
						getBitmapWorkerTask(imageView);
				if (this == bitmapWorkerTask && imageView != null) {
					imageView.setImageBitmap(bitmap);

				}
			}

			Log.i("SIZE", "CAHCED"+mMemoryCache.size());
		}
	}



	static class AsyncDrawable extends BitmapDrawable {
		private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

		public AsyncDrawable(Resources res, Bitmap bitmap,
				BitmapWorkerTask bitmapWorkerTask) {
			super(res, bitmap);
			bitmapWorkerTaskReference =
					new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
		}

		public BitmapWorkerTask getBitmapWorkerTask() {
			return bitmapWorkerTaskReference.get();
		}
	}

	public static boolean cancelPotentialWork(int data, ImageView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

		if (bitmapWorkerTask != null) {
			final int bitmapData = bitmapWorkerTask.data;
			if (bitmapData != data) {
				// Cancel previous task
				bitmapWorkerTask.cancel(true);
			} else {
				// The same work is already in progress
				return false;
			}
		}
		// No task associated with the ImageView, or an existing task was cancelled
		return true;
	}

	private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
		if (imageView != null) {
			final Drawable drawable = imageView.getDrawable();
			if (drawable instanceof AsyncDrawable) {
				final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
				return asyncDrawable.getBitmapWorkerTask();
			}
		}
		return null;
	}



	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize =1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);
			}
		}
		Log.i("", "Sample Size"+inSampleSize);
		return inSampleSize;
	}



	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inDither=false;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}





	public void addBitmapToCache(String key, Bitmap bitmap) {

		// Also add to disk cache
		synchronized (mDiskCacheLock) {
			if (mDiskLruCache != null && mDiskLruCache.get(key) == null) {
				mDiskLruCache.put(key, bitmap);
			}
		}
	}


	public Bitmap getBitmapFromDiskCache(String key) {
		synchronized (mDiskCacheLock) {
			// Wait while disk cache is started from background thread
			while (mDiskCacheStarting) {
				try {
					mDiskCacheLock.wait();
				} catch (InterruptedException e) {}
			}
			if (mDiskLruCache != null) {
				return mDiskLruCache.get(key);
			}
		}
		return null;
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}


	static class RetainFragment extends Fragment {
		private static final String TAG = "RetainFragment";
		public LruCache<String, Bitmap> mRetainedCache;

		public RetainFragment() {}

		public static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
			RetainFragment fragment = (RetainFragment) fm.findFragmentByTag(TAG);
			if (fragment == null) {
				fragment = new RetainFragment();
			}
			return fragment;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setRetainInstance(true);
		}
	}

}
