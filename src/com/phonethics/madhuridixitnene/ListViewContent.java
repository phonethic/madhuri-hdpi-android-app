package com.phonethics.madhuridixitnene;

import java.io.Serializable;
import java.util.ArrayList;

import android.util.Log;

public class ListViewContent implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String	thumbnailLink="";
	private String	title="";
	private String	contentWebLink="";
	private String 	shareUrl = "";
	private String	slug="";
	private int 	postId=0;
	private int 	pageState=0;
	private ArrayList<Integer> tempIdArr =new ArrayList<Integer>();
	
	
	
	public ListViewContent(){
		
	}

	
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
		tempIdArr.add(postId);
	}
	
	
	
	
	
	public int getPageState() {
		return pageState;
	}


	public void setPageState(int pageState) {
		this.pageState = pageState;
	}


	public String getThumbnailLink() {
		return thumbnailLink;
	}
	public void setThumbnailLink(String thumbnailLink) {
		this.thumbnailLink = thumbnailLink;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContentWebLink() {
		return contentWebLink;
	}
	public void setContentWebLink(String contentWebLink) {
		this.contentWebLink = contentWebLink;
	}
	public String getShareUrl() {
		return shareUrl;
	}
	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	
	public boolean isDuplicates(int id){
		for(int i=0; i<tempIdArr.size();i++){
			int tempId = tempIdArr.get(i);
			Log.i("---- TempID and id---", "-- TEmpID " + tempId + " --- ID " + id);
			if(tempId == id){
				
				return true;
				
			}
					
		}
		return false;
	}

}
