package com.phonethics.madhuridixitnene;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;


public class ImageFragment extends Fragment {

	Context context;
	File cacheDir;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	String url;
	ImageView prog;
	ImageLoaderConfiguration config;
	boolean fade=true;
	int position;
	public ImageFragment()
	{

	}
	public ImageFragment(Context context,String url,int position)
	{
		this.context=context;
		this.url=url;
		this.position=position;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		/*
		 * Setting Config for displaying image
		 */
		imageLoader =ImageLoader.getInstance();
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MadhuriGall");
		else
			cacheDir=context.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();
		

	/*	config= new ImageLoaderConfiguration.Builder(context)
		.threadPoolSize(3)
		.threadPriority(Thread.NORM_PRIORITY - 1)
		.denyCacheImageMultipleSizesInMemory()
		.offOutOfMemoryHandling()
		.discCacheExtraOptions(720, 480, CompressFormat.JPEG, 75)
		.discCache(new UnlimitedDiscCache(cacheDir)) 
		
		.discCacheFileNameGenerator(new HashCodeFileNameGenerator())
		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000)) // connectTimeout (120 s), readTimeout (120 s)
		.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
		.enableLogging()
		.build();*/
		config= new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.discCache(new UnlimitedDiscCache(cacheDir))
		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))
		.enableLogging()
		.build();
		imageLoader.init(config);


		options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		/*.displayer(new RoundedBitmapDisplayer(15))*/
		
		.build();



	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.imgelayout, container,false);
		TouchImageView imgView=(TouchImageView) view.findViewById(R.id.imageView1);
		imgView.setMaxZoom(2.0f);
		prog=(ImageView)view.findViewById(R.id.prog);


		//downloading and setting image to ImageView.
		imageLoader.displayImage(url, imgView, options,new ImageLoadingListener() {

			@Override
			public void onLoadingStarted() {
				// TODO Auto-generated method stub
				prog.startAnimation(AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate));

				prog.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(FailReason arg0) {
				// TODO Auto-generated method stub
				prog.clearAnimation();
				prog.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(Bitmap arg0) {
				// TODO Auto-generated method stub
				prog.clearAnimation();
				prog.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled() {
				// TODO Auto-generated method stub
				prog.clearAnimation();
				prog.setVisibility(View.GONE);
			}
		});

		/*imgView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(fade)
				{
					Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
					Gallery_Tab.header.startAnimation(anim);
					Gallery_Tab.bck.startAnimation(anim);
					anim.setFillAfter(true);
					Gallery_Tab.header.setVisibility(ViewGroup.GONE);
					Gallery_Tab.bck.setVisibility(ViewGroup.GONE);
					fade=false;
				}
				else if(!fade)
				{
					Animation anim=AnimationUtils.loadAnimation(context, R.anim.iconfade_in);
					Gallery_Tab.header.setVisibility(ViewGroup.VISIBLE);
					Gallery_Tab.bck.setVisibility(ViewGroup.VISIBLE);
					Gallery_Tab.header.startAnimation(anim);
					Gallery_Tab.bck.startAnimation(anim);
					anim.setFillAfter(true);
					fade=true;
				}	
			}
		});*/
		imgView.setTag(position);
		return view;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onActivityCreated(savedInstanceState);
	}



	

}

