package com.phonethics.madhuridixitnene;

import java.util.ArrayList;



import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class MainScreenImageFragment  extends Fragment {

	Context context;
	String imageName;

	public MainScreenImageFragment()
	{

	}
	public MainScreenImageFragment(Context context,String imageName)
	{
		this.context=context;
		this.imageName=imageName;
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);



	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.mainimagelayout, container,false);
		ImageView imgView=(ImageView)view.findViewById(R.id.mainImage);

		imgView.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imageName,"drawable",context.getPackageName())));


		return view;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onActivityCreated(savedInstanceState);
	}


}