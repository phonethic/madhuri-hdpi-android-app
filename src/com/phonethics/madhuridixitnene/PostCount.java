package com.phonethics.madhuridixitnene;

import java.io.Serializable;

public class PostCount implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private int postCount = 0;
	private String tabName = "";
	
	public PostCount(){}
	
	public int getPostCount() {
		return postCount;
	}
	public void setPostCount(int postCount) {
		this.postCount = postCount;
	}
	public String getTabName() {
		return tabName;
	}
	public void setTabName(String tabName) {
		this.tabName = tabName;
	}
	
	
}
