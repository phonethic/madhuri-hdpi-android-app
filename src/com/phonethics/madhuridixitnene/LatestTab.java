package com.phonethics.madhuridixitnene;



import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.PushService;


public class LatestTab extends Activity {

	Activity context;

	ArrayList<String> 						contentList,slugList,latestData,imgLinks;

	ArrayList<HashMap<String, String>> 		mylist;
	JSONObject 								json, obj,img,thumb;
	JSONArray  								latest, attachment;
	ImageView 								bck;
	String 									shareUrl = "http://madhuridixit-nene.com/latest/post/";
	String 									jString;
	ImageView 								showProgress;
	NetworkCheck 							netCheck;
	//File 									externalFile = new File(Environment.getExternalStorageDirectory(),"MadhuriLatest.bin");


	PullToRefreshListView 					listView;
	String 									imgStr,countArray;
	MyAsyncTask								downloadFile;
	ImageLoader 							loader;
	LazyAdapter 							lazyAdapter;
	String 									latestUrl = "http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=latest&count=-1";
	String									refreshUrl = "http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=latest&count=1";

	Bundle 									bundle;
	boolean 								isFinish = true, errorCaught = false;
	boolean 								isRefreshing = false, refreshCall=false;;
	Dialog 									dialog;
	int										presentSize=0, newSize,dataSize=0;
	boolean									fade, noFileandInternet = false;
	int										CACHE_VALIDITY;

	JSONObject								objJson,configJson,tempJson;
	JSONArray								categoryJson;
	ImageView 								btnLoadMore;
	ObjectSerializer						objSerial;
	ListViewContent							listContetnt;
	PostCount								postCount;
	TabConfig								tabAsync;
	String 									latestUrlWithCount = "http://madhuridixit-nene.com/wp/?json=get_category_posts&dev=1&slug=latest&page=";
	String									TAB_INFO ="/mnt/sdcard/MdNTabpostConfig.bin";
	String									TABINFO_URL="http://madhuridixit-nene.com/wp/?json=get_category_index&dev=1";
	File 									serialFile = new File(Environment.getExternalStorageDirectory(),"MdNLatest.bin");
	File									configFile= new File(Environment.getExternalStorageDirectory(),"MdNTabpostConfig.bin");

	String 									MDLATEST="/mnt/sdcard/MdNLatest.bin";


	int										pageCount=1,totalPages,TOTAL_POST_COUNT;
	ArrayList<Integer> 						postID;
	ArrayList<ListViewContent> 				listObject;
	ArrayList<PostCount> 					postConfigArrray;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*this.requestWindowFeature(Window.FEATURE_NO_TITLE);*/
		setContentView(R.layout.activity_latest_tab);

		context		=		this;
		/*PushService.setDefaultPushCallback(context, LatestTab.class);*/

		bck 		= 		(ImageView) findViewById(R.id.bckbtn);
		showProgress=		(ImageView)findViewById(R.id.showProgress);
		netCheck 	= 		new NetworkCheck(context);
		listView	=       (PullToRefreshListView) findViewById(R.id.latestList);
		latestData	=		new ArrayList<String>();
		imgLinks	=		new ArrayList<String>();
		mylist 		= 		new ArrayList<HashMap<String, String>>();
		contentList	=		new ArrayList<String>();
		slugList	=		new ArrayList<String>();


		mylist 				= new ArrayList<HashMap<String, String>>();
		contentList			= new ArrayList<String>();
		slugList			= new ArrayList<String>();
		lazyAdapter			= new LazyAdapter(context);

		objSerial	=		new ObjectSerializer();
		btnLoadMore = 		(ImageView) findViewById(R.id.btnLoadMore);
		postID		=		new ArrayList<Integer>();
		listObject			= new ArrayList<ListViewContent>();
		postConfigArrray	= new  ArrayList<PostCount>();
		tabAsync			= new TabConfig();

		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		listView.setLayoutAnimation(controller);


		dialog 		= new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertbox);

		CACHE_VALIDITY = Integer.parseInt(getResources().getString(R.string.CacheFileValidity));

		listView.setVisibility(View.GONE);

		downloadFile = new MyAsyncTask();

		bundle = getIntent().getExtras();
		if(bundle != null){
			isFinish = bundle.getBoolean("finishAct");

			fade=bundle.getBoolean("shtcut");
		}

		if(netCheck.isNetworkAvailable()){ 

			/*
			 *  Getting total post count and serailizing it
			 */

			if(!configFile.exists() || configFile.length()==0){
				/*
				 *  If configFile Dosent exist or file length is zero then serialize it.
				 */
				tabAsync.execute(TABINFO_URL);
			}else{
				/*
				 * Deserializing  and store latest post count
				 */
				Object deserialTabInfo =  objSerial.deserialize(TAB_INFO);
				postConfigArrray = (ArrayList<PostCount>)deserialTabInfo;
				for(int i=0;i<postConfigArrray.size();i++){
					if(postConfigArrray.get(i).getTabName().equalsIgnoreCase("Latest")){
						TOTAL_POST_COUNT = postConfigArrray.get(i).getPostCount();

					}
				}

				Log.i("*******", "Total POst " + TOTAL_POST_COUNT);
			}

			if(serialFile.exists()){

				/*
				 *  if network is available and file exist deserialize it 
				 */

				Object	deserial = objSerial.deserialize(MDLATEST);
				listObject=(ArrayList<ListViewContent>)deserial;
				for(int r=0; r<listObject.size();r++){
					imgLinks.add(listObject.get(r).getThumbnailLink());
					latestData.add(listObject.get(r).getTitle());
					slugList.add(listObject.get(r).getSlug());
					contentList.add(listObject.get(r).getContentWebLink());
					pageCount = listObject.get(r).getPageState();
					postID.add(listObject.get(r).getPostId());
				}

				lazyAdapter = new LazyAdapter(context, imgLinks, latestData);
				listView.setAdapter(lazyAdapter);
				listView.setVisibility(View.VISIBLE);
				lazyAdapter.notifyDataSetChanged();
				if(latestData.size()< TOTAL_POST_COUNT){
					btnLoadMore.setVisibility(View.VISIBLE);
				}
				Log.i("******0---*", "lateset Data " + latestData.size());
			}else{
				/*
				 *  net connected but no such file exist then download content
				 */


				Log.d("************", "-------EXTERNAL FILOE WAS NOT THERE DOWNLOADING NEW ONE-------");
				showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				showProgress.setVisibility(View.VISIBLE);
				downloadFile.execute(latestUrlWithCount+pageCount);
			}
		}else{

			/*
			 * 
			 *  If net is not connected and file exist
			 */
			if(serialFile.exists()){
				Object	deserial = objSerial.deserialize(MDLATEST);
				listObject=(ArrayList<ListViewContent>)deserial;
				for(int r=0; r<listObject.size();r++){
					imgLinks.add(listObject.get(r).getThumbnailLink());
					latestData.add(listObject.get(r).getTitle());
					slugList.add(listObject.get(r).getSlug());
					contentList.add(listObject.get(r).getContentWebLink());
					pageCount = listObject.get(r).getPageState();
					postID.add(listObject.get(r).getPostId());
				}
				lazyAdapter = new LazyAdapter(context, imgLinks, latestData);
				listView.setAdapter(lazyAdapter);
				listView.setVisibility(View.VISIBLE);
				lazyAdapter.notifyDataSetChanged();

			}else{
				noFileandInternet = true;
				Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			}
		}

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		

				/*
				 * Opening Link in WebView
				 */
				if(!isRefreshing){
					Intent intent=new Intent(context,WebViewActivity.class);
					intent.putExtra("content", contentList.get(position-1));
					intent.putExtra("shareUrl", shareUrl+slugList.get(position-1));
					intent.putExtra("tab_value", 1);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});

		/*
		 * Loading More records 
		 */
		btnLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(   (!isRefreshing) && (!showProgress.isShown())  ){
					if(pageCount < totalPages || totalPages == 0){
						pageCount += 1;

						showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
						showProgress.setVisibility(View.VISIBLE);
						new MyAsyncTask().execute(latestUrlWithCount+pageCount);
					}
				}
			}
		});


		bck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				if(showProgress.isShown() || isRefreshing){

					/*//final Dialog dialog = new Dialog(LAtestTab.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
					dialog.setContentView(R.layout.alertbox);*/

					ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
					ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);
					dialog.show();


					yes.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v) 
						{


							if(listObject.size()!=0){
								objSerial.serialize((Object)listObject,MDLATEST);
							}
							/*							lazyAdapter.clear();*/
							/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
							LazyAdapter.imageloader.clearCache();
							if(isFinish){


								dialog.dismiss();
								LazyAdapter.imageloader.clearCache();
								if(lazyAdapter!=null)
								{
									LazyAdapter.clear();
								}
								downloadFile.cancel(true);

								Intent intent = new Intent(context,MadhuriHomeScreen.class);
								startActivity(intent);
								/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/


								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}

								finish();
							}else{


								dialog.dismiss(); 

								LazyAdapter.imageloader.clearCache();
								downloadFile.cancel(true);

								if(lazyAdapter!=null)
								{
									LazyAdapter.clear();
								}

								finish();
								/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/


								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}
							}
						}
					});

					no.setOnClickListener(new OnClickListener()
					{


						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub


							dialog.dismiss();  
						}
					});

					/*dialog.show();*/
				}
				else{
					if(listObject.size()!=0){
						objSerial.serialize((Object)listObject,MDLATEST);
					}
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					LazyAdapter.imageloader.clearCache();

					if(lazyAdapter!=null)
					{
						LazyAdapter.clear();
					}
					downloadFile.cancel(true);
					finish();
					/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/


					if(fade)
					{
						overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					}
					else
					{
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}
			}
		});


		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel

				/*refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);*/

				// Do work to refresh the list here.
				if(netCheck.isNetworkAvailable()){
					isRefreshing = true;

					/*			

					try {
						FileInputStream filename 				= null;
						filename = new FileInputStream("/sdcard/MadhuriLatest.bin");

						FileChannel fc = filename.getChannel();
						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						jString = Charset.defaultCharset().decode(bb).toString();
						json = new JSONObject(jString); 
						objJson = json.getJSONObject("category");
						presentSize = objJson.getInt("post_count");
						latest = json.getJSONArray("posts");
						presentSize = latest.length();

					}catch(JSONException e){
						Log.e("log_tag", "Error parsing data "+e.toString());

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					catch (Exception e){
						Log.e("Error: ", " "+e.getMessage());
					}
					latestUrl = "http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=latest&count=1";

					new MyAsyncTask().execute(latestUrl);


					 */					
					/*if(serialFile.exists()){
						Object	deserial = objSerial.deserialize("/mnt/sdcard/MdLatest.bin");
						listObject=(ArrayList<ListViewContent>)deserial;
						imgLinks.clear();
						latestData.clear();
						slugList.clear();
						contentList.clear();
						for(int r=0; r<listObject.size();r++){
							imgLinks.add(listObject.get(r).getThumbnailLink());
							latestData.add(listObject.get(r).getTitle());
							slugList.add(listObject.get(r).getSlug());
							contentList.add(listObject.get(r).getContentWebLink());
							pageCount = listObject.get(r).getPageState();
						}



					}*/
					if(!showProgress.isShown()){
						new TabConfig().execute(TABINFO_URL);
					}else{
						listView.onRefreshComplete();
					}


				}else{
					listView.onRefreshComplete();
				}

			}
		});



	}

	class MyAsyncTask extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>> > {
		/*mylist = new ArrayList<HashMap<String, String>>();*/
		boolean exceptionOccured  = false;


		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {
			// TODO Auto-generated method stub
			int count;
			InputStream is = null;
			String result = "";
			JSONObject jObj = null;

			/*json 				= new JSONObject();*/

			try{


				try{
					/*DefaultHttpClient  httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(params[0]);*/

					HttpGet httpGet = new HttpGet(params[0]);
					HttpParams httpParams = new BasicHttpParams();

					int timeoutConnection = 30000;
					HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 30000;
					HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

					DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
					HttpResponse response = httpClient.execute(httpGet);

					HttpEntity entity = response.getEntity();
					is = entity.getContent();

				}catch(Exception ex){
					Log.e("log_tag", "Error in http connection "+ex.toString());
					ex.printStackTrace();
					exceptionOccured = true;
				}

				try{
					if(!exceptionOccured){
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						StringBuilder	sb  = new StringBuilder();
						String line = null;

						while ((line = reader.readLine())!=null) {
							sb.append(line + "\n");

						}
						is.close();
						result = sb.toString();
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error converting result "+ex.toString());
					exceptionOccured = true;
				}

				//try parse the string to a JSON object
				try{
					if(!exceptionOccured){
						jObj = new JSONObject(result);
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error parsing data "+ex.toString());
					exceptionOccured = true;
				}


				if(!exceptionOccured){

					//json 		= JSONfunctions.getJSONfromURL(params[0]);
					objJson 	= jObj.getJSONObject("category");
					dataSize 	= objJson.getInt("post_count");
					totalPages = jObj.getInt("pages");
					Log.d("!!!!!!!!!!!!!!!", "Count " + dataSize);

					latest  = jObj.getJSONArray("posts");
					if(isRefreshing && pageCount == 1){
						contentList.clear();
						slugList.clear();
						latestData.clear();
						imgLinks.clear();
						postID.clear();
						listObject.clear();
					}
					for(int i=0;i<latest.length();i++){	

						HashMap<String, String> map = new HashMap<String, String>();	
						JSONObject e = latest.getJSONObject(i);

						String name = e.getString("title_plain");
						String content = e.getString("content");
						int postId = e.getInt("id");

						attachment = e.getJSONArray("attachments");
						for(int p=0; p<attachment.length();p++){
							/*obj = attachment.getJSONObject(p);
						img = obj.getJSONObject("images");
						thumb = img.getJSONObject("thumbnail");
						imgStr = thumb.getString("url");*/

							obj = attachment.getJSONObject(p);
							img = null;
							try{
								img = obj.getJSONObject("images");
							}catch(Exception ex){
								ex.printStackTrace();
							}
							if(img!=null){
								thumb= null;
								try{
									thumb = img.getJSONObject("thumbnail");
								}catch(Exception ex){
									ex.printStackTrace();
								}
								if(thumb!=null){
									imgStr = thumb.getString("url");
									Log.d("--------------","madhuri url " + imgStr);
								}else{
									imgStr="";
								}	
							}else{
								imgStr="";	
							}
						}

						Log.d("--------------",imgStr);


						String 	fnl_Str="";
						String slug = "";

						if((postID.size()== 0) || (!postID.contains(postId))){
							contentList.add(content);
							String chng = "&#8211;";

							fnl_Str = name.replace(chng, "");
							slug = e.getString("slug");

							slugList.add(slug);
							latestData.add(fnl_Str);
							imgLinks.add(imgStr);
							postID.add(postId);

							listContetnt		= new ListViewContent();
							listContetnt.setContentWebLink(content);
							listContetnt.setPostId(postId);
							listContetnt.setThumbnailLink(imgStr);
							listContetnt.setTitle(fnl_Str);
							listContetnt.setSlug(slug);
							listContetnt.setShareUrl(shareUrl);
							listContetnt.setPageState(pageCount);
							listObject.add(listContetnt);
						}

						map.put("id",  String.valueOf(i));
						map.put("name", fnl_Str);
						map.put("date", e.getString("date"));
						/*shareUrl ="http://madhuridixit-nene.com/latest/post/";*/

						/*	listContetnt		= new ListViewContent();*/

						/*if(!listContetnt.isDuplicates(postId)){
						listContetnt.setContentWebLink(content);
						listContetnt.setPostId(postId);
						listContetnt.setThumbnailLink(imgStr);
						listContetnt.setTitle(fnl_Str);
						listContetnt.setSlug(slug);
						listContetnt.setShareUrl(shareUrl);
						listContetnt.setPageState(pageCount);
						listObject.add(listContetnt);
					}*/

						mylist.add(map);

					}
					/*}else{
					Log.d("*************************************", "NO NEW DATA AVAILBALE");
					Log.d("****", "Present Size " + presentSize + "Data Size " + dataSize);
					listView.onRefreshComplete();
				}*/
				}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}

			return mylist;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {

			if(exceptionOccured){
				Toast.makeText(context, "Network Error", 0).show();
				listView.onRefreshComplete();
			}else{
				if(latestData.size() < TOTAL_POST_COUNT){
					btnLoadMore.setVisibility(View.VISIBLE);
				}else{
					btnLoadMore.setVisibility(View.GONE);
				}
				btnLoadMore.setClickable(true);
				listView.setVisibility(View.VISIBLE);
				showProgress.clearAnimation();
				showProgress.setVisibility(View.GONE);
				isRefreshing = false;
				Log.i("List Data Size-----------------", "List Data Size " + listObject.size());
				if(dialog.isShowing()){
					dialog.dismiss();
				}

				if(errorCaught){
					Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
				}else{
					lazyAdapter = null;
					lazyAdapter = new LazyAdapter(context, imgLinks, latestData);
					listView.setAdapter(lazyAdapter);
					lazyAdapter.notifyDataSetChanged();

				}

				if(pageCount == totalPages){
					btnLoadMore.setVisibility(View.GONE);
				}else{
					btnLoadMore.setVisibility(View.VISIBLE);
				}


				listView.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		

						if(!isRefreshing)
						{
							Intent intent=new Intent(context,WebViewActivity.class);
							intent.putExtra("content", contentList.get(position-1));
							intent.putExtra("shareUrl", shareUrl+slugList.get(position-1));
							intent.putExtra("tab_value", 1);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}

					}
				});

				listView.onRefreshComplete();
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			/*
			 * Disabling Clicks of ListView And Load More Button
			 */
			listView.setOnItemClickListener(null);
			btnLoadMore.setClickable(false);
			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub

			super.onProgressUpdate(values);

		}





	}//My Async Ends


	class TabConfig extends AsyncTask<String, Integer, ArrayList<HashMap<String, Integer>> >{

		boolean exceptionOccured  = false;

		@Override
		protected ArrayList<HashMap<String, Integer>> doInBackground(
				String... params) {

			// TODO Auto-generated method stub
			InputStream is = null;
			String result = "";
			JSONObject jObj = null;



			try {


				//configJson 	= JSONfunctions.getJSONfromURL(params[0]);

				//http post
				try{
					/*DefaultHttpClient  httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(params[0]);*/

					HttpGet httpGet = new HttpGet(params[0]);
					HttpParams httpParams = new BasicHttpParams();

					int timeoutConnection = 30000;
					HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 30000;
					HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

					DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
					HttpResponse response = httpClient.execute(httpGet);

					HttpEntity entity = response.getEntity();
					is = entity.getContent();

				}catch(Exception ex){
					Log.e("log_tag", "Error in http connection "+ex.toString());
					ex.printStackTrace();
					exceptionOccured = true;
				}



				//convert response to string
				try{
					if(!exceptionOccured){
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						StringBuilder	sb  = new StringBuilder();
						String line = null;

						while ((line = reader.readLine())!=null) {
							sb.append(line + "\n");

						}
						is.close();
						result = sb.toString();
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error converting result "+ex.toString());
					exceptionOccured = true;
				}

				//try parse the string to a JSON object
				try{
					if(!exceptionOccured){
						jObj = new JSONObject(result);
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error parsing data "+ex.toString());
					exceptionOccured = true;
				}


				if(!exceptionOccured){
					categoryJson	=	jObj.getJSONArray("categories");
					for(int i=0; i <categoryJson.length(); i++){
						tempJson = categoryJson.getJSONObject(i);
						String title = tempJson.getString("title");

						int post_count = tempJson.getInt("post_count");
						if(title.equalsIgnoreCase("Latest")){
							TOTAL_POST_COUNT = post_count;
						}
						Log.i("*** Asynch **", "Total Post " + TOTAL_POST_COUNT);
						postCount = new PostCount();
						postCount.setPostCount(post_count);
						postCount.setTabName(title);
						postConfigArrray.add(postCount);
					}
				}
			} catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, Integer>> result) {
			// TODO Auto-generated method stub

			if(exceptionOccured){
				Toast.makeText(context, "Network Error", 0).show();
				listView.onRefreshComplete();
			}else{
				objSerial.serialize((Object)postConfigArrray, TAB_INFO);
				for(int i=0;i<postConfigArrray.size();i++){
					if(postConfigArrray.get(i).getTabName().equalsIgnoreCase("Latest")){
						TOTAL_POST_COUNT = postConfigArrray.get(i).getPostCount();

					}
				}
				/*if(latestData.size()< TOTAL_POST_COUNT){
				
					if(isRefreshing){
						pageCount = 1;
						new MyAsyncTask().execute(latestUrlWithCount+pageCount);
					}
					
				}*/
				if(isRefreshing){
					pageCount = 1;
					new MyAsyncTask().execute(latestUrlWithCount+pageCount);
				}else{
					btnLoadMore.setVisibility(View.GONE);
					listView.onRefreshComplete();
					isRefreshing = false;
					if(dialog.isShowing()){
						dialog.dismiss();
					}
				}
			}
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			super.onPreExecute();
		}


	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_latest_tab, menu);
		return false;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub


		if(showProgress.isShown() || isRefreshing){


			ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
			ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);

			dialog.show();


			yes.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v) 
				{
					if(listObject.size()!=0){
						objSerial.serialize((Object)listObject,MDLATEST);
					}

					/*							lazyAdapter.clear();*/
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					if(isFinish){

						dialog.dismiss();
						LazyAdapter.imageloader.clearCache();
						if(lazyAdapter!=null)
						{
							LazyAdapter.clear();
						}
						downloadFile.cancel(true);

						Intent intent = new Intent(context,MadhuriHomeScreen.class);
						startActivity(intent);
						/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/


						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}

						finish();
					}else{

						dialog.dismiss(); 
						LazyAdapter.imageloader.clearCache();
						if(lazyAdapter!=null)
						{
							LazyAdapter.clear();
						}

						downloadFile.cancel(true);
						finish();
						/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/


						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
					}
				}
			});

			no.setOnClickListener(new OnClickListener()
			{


				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					dialog.dismiss();  
				}
			});

			/*dialog.show();*/
		}
		else{


			if(listObject.size()!=0){
				objSerial.serialize((Object)listObject,MDLATEST);
			}
			/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
			LazyAdapter.imageloader.clearCache();
			if(lazyAdapter!=null)
			{
				LazyAdapter.clear();
			}
			super.onBackPressed();
			/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/


			if(fade)
			{
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
			}
			else
			{
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}



	}


	@Override
	public void finish() {
		// TODO Auto-generated method stub
		try{
			LazyAdapter.imageloader.clearCache();
			Log.i("^^^^^^ In Finish  ", "clear chache ");
		}catch(NullPointerException npEx){
			npEx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.finish();
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		/*
		 * Persisting State of List
		 */
		try{
			if(listObject.size()!=0){
				objSerial.serialize((Object)listObject,MDLATEST);
			}
		}catch(NullPointerException np){
			np.printStackTrace();
		}catch(Exception xp){
			xp.printStackTrace();
		}
		super.onPause();
	}




}


