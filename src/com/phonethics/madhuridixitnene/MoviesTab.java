package com.phonethics.madhuridixitnene;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;

import com.parse.PushService;
import com.phonethics.madhuridixitnene.LatestTab.MyAsyncTask;

public class MoviesTab extends Activity {

	Activity				context;

	static final String url			= "http://madhuridixit-nene.com/uploads/filmography.xml";
	static final String KEY_ITEM 	= "movie"; // parent node
	String 				xml;
	Document			doc;
	NodeList 			nl;
	DefaultHttpClient 	httpClient;
	HttpPost 			httpPost;
	HttpResponse		httpRes;
	HttpEntity			httpEnt;
	FileFromURL			downloadFile;
	ArrayList<String> 	photoLink;

	ArrayList<String> 	mYear;
	ArrayList<String> 	mName;
	ArrayList<String> 	mImage;
	ArrayList<String> 	mAwards;
	ArrayList<String> 	mDirector;
	ArrayList<String> 	mProducer;
	ArrayList<String> 	mActedAs;
	ArrayList<String> 	mAnecdotes;
	ArrayList<String> 	mSynopsis;
	/*CustomListAdapter 	adapter;*/
	NetworkCheck netAvailable;

	static ImageLoader imageloader;
	boolean refreshCount=false;
	Dialog dialog;
	boolean									fade;


	/*	File cacheDir;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;*/

	int					tempLength;

	File externalFile 		= new File(Environment.getExternalStorageDirectory(),"MadhuriMovies.xml");
	String xmlurl			= "http://madhuridixit-nene.com/uploads/filmography.xml";
	String shareUrl 		="http://madhuridixit-nene.com/uploads/films/";

	PullToRefreshListView listView;
	ImageView showProgress;
	ImageView bck;

	LazyAdapter lazyadapter;
	Bundle bundle;
	boolean isFinish = true, errorCaught = false, isRefreshing = false, refreshDataCall=false;

	int									presentSize=0, newSize,dataSize=0;
	int										CACHE_VALIDITY;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movies_tab);

		context		= this;
		/*PushService.setDefaultPushCallback(context, MoviesTab.class);*/

		mYear		=	new ArrayList<String>();
		mName		=	new ArrayList<String>();
		mImage		=	new ArrayList<String>();
		mAwards		=	new ArrayList<String>();
		mDirector	=	new ArrayList<String>();
		mProducer	=	new ArrayList<String>();
		mActedAs	=	new ArrayList<String>();
		mAnecdotes	=	new ArrayList<String>();
		mSynopsis	=	new ArrayList<String>();
		lazyadapter			= new LazyAdapter(context);


		listView = (PullToRefreshListView) findViewById(R.id.moviesList);

		showProgress=(ImageView)findViewById(R.id.showProgress);
		bck = (ImageView) findViewById(R.id.bckbtn);
		dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertbox);

		CACHE_VALIDITY = Integer.parseInt(getResources().getString(R.string.CacheFileValidity));

		downloadFile = new FileFromURL();

		listView.setVisibility(View.GONE);

		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		listView.setLayoutAnimation(controller);

		netAvailable = new NetworkCheck(context);
		bundle = getIntent().getExtras();
		if(bundle != null){
			isFinish = bundle.getBoolean("finishAct");

			fade=bundle.getBoolean("shtcut");
		}
		if(netAvailable.isNetworkAvailable()){
			if(externalFile.exists()){
				Date lastModDate = new Date(externalFile.lastModified());
				Date todayDate = new Date();
				long fileDate = lastModDate.getTime();
				long currentDate = todayDate.getTime();
				long dateDiff = currentDate - fileDate;
				long diffDays = dateDiff / (24 * 60 * 60 * 1000);
				if(diffDays > CACHE_VALIDITY){
					Log.d("************", "-------NO OF DAYS DIFFERENCE-------" +diffDays);
					showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
					showProgress.setVisibility(View.VISIBLE);
					downloadFile.execute(xmlurl);					
				}else{
					try {
						InputStream filename 	= null;
						Document xmldoc 		= null;
						DocumentBuilderFactory doc_build_fact 	= null;
						DocumentBuilder doc_builder = null;
						filename = new FileInputStream("/sdcard/MadhuriMovies.xml");

						//Toast.makeText(context, "FILE EXITs", Toast.LENGTH_SHORT).show();
						doc_build_fact = DocumentBuilderFactory.newInstance();

						doc_builder = doc_build_fact.newDocumentBuilder();
						xmldoc = doc_builder.parse(filename);
						NodeList nodeList = null;

						nodeList = xmldoc.getElementsByTagName("movie");
						tempLength = nodeList.getLength();
						dataSize = nodeList.getLength();
						mImage.clear();
						for(int i=0; i<nodeList.getLength();i++){
							Node node = nodeList.item(i);
							Element fstElmnt = (Element) node;

							mYear.add(fstElmnt.getAttribute("Year"));
							mName.add(fstElmnt.getAttribute("Name"));
							mImage.add(shareUrl+fstElmnt.getAttribute("Images"));
							mAwards.add(fstElmnt.getAttribute("Awards"));
							mDirector.add(fstElmnt.getAttribute("Director"));
							mProducer.add(fstElmnt.getAttribute("Producer"));
							mActedAs.add(fstElmnt.getAttribute("Acted_As"));
							mAnecdotes.add(fstElmnt.getAttribute("Anecdotes"));
							mSynopsis.add(fstElmnt.getAttribute("Synopsis"));


						}

						for(int i=0;i<tempLength;i++){
							Log.d("Movie---" + i,mYear.get(i) + "----" + 
									mName.get(i) + "----" + 
									mImage.get(i)+ "----" +
									mAwards.get(i)+ "----" +
									mDirector.get(i) + "----" +
									mProducer.get(i) + "----" +
									mActedAs.get(i) + "----" +
									mAnecdotes.get(i) + "----" +
									mSynopsis.get(i) + "----" );
						}


						lazyadapter = new LazyAdapter(context, mImage, mName);
						listView.setAdapter(lazyadapter);
						listView.setVisibility(View.VISIBLE);
						lazyadapter.notifyDataSetChanged();


					}
					catch (SAXException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}else{
				Log.d("************", "-------EXTERNAL FILE WAS NOT THERE DOWNLOADING NEW ONE-------");
				showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				showProgress.setVisibility(View.VISIBLE);
				downloadFile.execute(xmlurl);
			}
		}else{
			//Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			if(externalFile.exists()){
				try {
					InputStream filename 	= null;
					Document xmldoc 		= null;
					DocumentBuilderFactory doc_build_fact 	= null;
					DocumentBuilder doc_builder = null;
					filename = new FileInputStream("/sdcard/MadhuriMovies.xml");

					//Toast.makeText(context, "FILE EXITs", Toast.LENGTH_SHORT).show();
					doc_build_fact = DocumentBuilderFactory.newInstance();

					doc_builder = doc_build_fact.newDocumentBuilder();
					xmldoc = doc_builder.parse(filename);
					NodeList nodeList = null;

					nodeList = xmldoc.getElementsByTagName("movie");
					tempLength = nodeList.getLength();
					dataSize = nodeList.getLength();
					mImage.clear();
					for(int i=0; i<nodeList.getLength();i++){
						Node node = nodeList.item(i);
						Element fstElmnt = (Element) node;

						mYear.add(fstElmnt.getAttribute("Year"));
						mName.add(fstElmnt.getAttribute("Name"));
						mImage.add(shareUrl+fstElmnt.getAttribute("Images"));
						mAwards.add(fstElmnt.getAttribute("Awards"));
						mDirector.add(fstElmnt.getAttribute("Director"));
						mProducer.add(fstElmnt.getAttribute("Producer"));
						mActedAs.add(fstElmnt.getAttribute("Acted_As"));
						mAnecdotes.add(fstElmnt.getAttribute("Anecdotes"));
						mSynopsis.add(fstElmnt.getAttribute("Synopsis"));


					}

					for(int i=0;i<tempLength;i++){
						Log.d("Movie---" + i,mYear.get(i) + "----" + 
								mName.get(i) + "----" + 
								mImage.get(i)+ "----" +
								mAwards.get(i)+ "----" +
								mDirector.get(i) + "----" +
								mProducer.get(i) + "----" +
								mActedAs.get(i) + "----" +
								mAnecdotes.get(i) + "----" +
								mSynopsis.get(i) + "----" );
					}


					lazyadapter = new LazyAdapter(context, mImage, mName);
					listView.setAdapter(lazyadapter);
					listView.setVisibility(View.VISIBLE);
					lazyadapter.notifyDataSetChanged();


				}
				catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			}

		}



		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				//Toast.makeText(context, mName.get(position), Toast.LENGTH_SHORT).show();
				if(!isRefreshing){
					Intent intent=new Intent(context,WebViewActivity.class);
					intent.putExtra("tab_value", 3);
					intent.putExtra("movieYear", mYear.get(position-1));
					intent.putExtra("movieName", mName.get(position-1));
					intent.putExtra("movieImage",mImage.get(position-1));
					intent.putExtra("movieAwards", mAwards.get(position-1));
					intent.putExtra("movieDirector", mDirector.get(position-1));
					intent.putExtra("movieProducer", mProducer.get(position-1));
					intent.putExtra("movieActedAs", mActedAs.get(position-1));
					intent.putExtra("movieAnecdotes", mAnecdotes.get(position-1));
					intent.putExtra("movieSynopsis", mSynopsis.get(position-1));
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});

		bck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(showProgress.isShown()|| isRefreshing){

					/*//final Dialog dialog = new Dialog(LAtestTab.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
					dialog.setContentView(R.layout.alertbox);
					 */
					ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
					ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);

					yes.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v) 
						{
							/*					lazyAdapter.clear();*/
							/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
							
							if(isFinish){
								dialog.dismiss();
								LazyAdapter.imageloader.clearCache();
								if(lazyadapter!=null)
								{
									LazyAdapter.clear();
								}
								Intent intent = new Intent(context,MadhuriHomeScreen.class);
								startActivity(intent);

								/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/

								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}
								downloadFile.cancel(true);
								finish();

							}else{
								dialog.dismiss();
								LazyAdapter.imageloader.clearCache();
								if(lazyadapter!=null)
								{
									LazyAdapter.clear();
								}
								downloadFile.cancel(true);
								finish();
								/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}
							}
						}
					});

					no.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v) 
						{
							dialog.dismiss();  

						}
					});

					dialog.show();
				}
				else{
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					LazyAdapter.imageloader.clearCache();
					if(lazyadapter!=null)
					{
						LazyAdapter.clear();
					}
					downloadFile.cancel(true);
					finish();
					/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
					if(fade)
					{
						overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					}
					else
					{
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}
			}
		});


		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				/*refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);*/

				// Do work to refresh the list here.
				if(netAvailable.isNetworkAvailable()){
					isRefreshing = true;
					if(externalFile.exists()){
						try {
							InputStream filename 	= null;
							Document xmldoc 		= null;
							DocumentBuilderFactory doc_build_fact 	= null;
							DocumentBuilder doc_builder = null;
							filename = new FileInputStream("/sdcard/MadhuriMovies.xml");

							//Toast.makeText(context, "FILE EXITs", Toast.LENGTH_SHORT).show();
							doc_build_fact = DocumentBuilderFactory.newInstance();

							doc_builder = doc_build_fact.newDocumentBuilder();
							xmldoc = doc_builder.parse(filename);
							NodeList nodeList = null;

							nodeList = xmldoc.getElementsByTagName("movie");
							tempLength = nodeList.getLength();
							presentSize = nodeList.getLength();
						}catch (SAXException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ParserConfigurationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (Exception e){
							Log.e("Error: ", " "+e.getMessage());
						}

					}	
					new FileFromURL().execute(xmlurl);
					refreshCount=true;
				}else{
					listView.onRefreshComplete();
				}
			}
		});




	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_movies_tab, menu);
		return false;
	}



	class FileFromURL extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub
			int count;


			try {
				
				HttpParams httpParams = new BasicHttpParams();

				int timeoutConnection = 30000;
				HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT) 
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);
				
				
				httpClient 	=	new DefaultHttpClient(httpParams);
				httpPost 	= 	new HttpPost(xmlurl);

				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
				DocumentBuilder	db	= dbf.newDocumentBuilder();
				InputSource		is	= new InputSource();
				is.setCharacterStream(new StringReader(xml));
				doc					= db.parse(is);
				nl	= doc.getElementsByTagName("movie");
				dataSize = nl.getLength();
				if(dataSize != presentSize){
					refreshDataCall = true;

					URL url = new URL(xmlur[0]);
					URLConnection connection = url.openConnection();
					connection.connect();

					// getting file length
					int lenghtOfFile = connection.getContentLength();

					// input stream to read file - with 8k buffer
					InputStream input = new BufferedInputStream(url.openStream(), 8192);

					// Output stream to write file
					FileOutputStream output = new FileOutputStream("/sdcard/MadhuriMovies.xml");


					byte data[] = new byte[1024];

					long total = 0;
					while (   ((count = input.read(data)) != -1)  ) {
						total += count;

						// publishing the progress....
						// After this onProgressUpdate will be called
						/*publishProgress(""+(int)((total*100)/lenghtOfFile));*/

						// writing data to file
						output.write(data, 0, count);
					}

					output.flush();
					// closing streams
					output.close();
					input.close();



				}else{
					refreshDataCall = false;
					Log.d("*************************************", "NO NEW Movie DATA AVAILBALE");
				}
			}
			catch(SocketException socketException){
				Log.d("***", "Socket Exception");
				errorCaught = true;
			}catch(ConnectionClosedException con){
				errorCaught = true;
			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");
				errorCaught = true;
			}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}



			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub

			showProgress.clearAnimation();
			showProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			isRefreshing = false;
			if(dialog.isShowing()){
				dialog.dismiss();
			}

			if(errorCaught){
				Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
				listView.onRefreshComplete();
			}else{
				try{


					DocumentBuilderFactory 	dbf	=	DocumentBuilderFactory.newInstance();
					DocumentBuilder	db	= dbf.newDocumentBuilder();
					InputSource		is	= new InputSource();
					is.setCharacterStream(new StringReader(xml));
					doc					= db.parse(is);
					nl	= doc.getElementsByTagName("movie");
					tempLength = nl.getLength();

					if(refreshDataCall){
						Log.d("*************************************", "NEW  DATA AVAILBALE");
						mImage.clear();
						mName.clear();

						for(int i=0; i < tempLength; i++){
							Node node = nl.item(i);
							Element fstElmnt = (Element) node;


							mYear.add(fstElmnt.getAttribute("Year"));
							mName.add(fstElmnt.getAttribute("Name"));
							mImage.add(shareUrl + fstElmnt.getAttribute("Images"));
							mAwards.add(fstElmnt.getAttribute("Awards"));
							mDirector.add(fstElmnt.getAttribute("Director"));
							mProducer.add(fstElmnt.getAttribute("Producer"));
							mActedAs.add(fstElmnt.getAttribute("Acted_As"));
							mAnecdotes.add(fstElmnt.getAttribute("Anecdotes"));
							mSynopsis.add(fstElmnt.getAttribute("Synopsis"));
						}

						for(int i=0;i<tempLength;i++){
							Log.d("Movie---" + i,mYear.get(i) + "----" + 
									mName.get(i) + "----" + 
									mImage.get(i)+ "----" +
									mAwards.get(i)+ "----" +
									mDirector.get(i) + "----" +
									mProducer.get(i) + "----" +
									mActedAs.get(i) + "----" +
									mAnecdotes.get(i) + "----" +
									mSynopsis.get(i) + "----" );
						}
						if(refreshCount){
							LazyAdapter.clear();
						}
						lazyadapter=null;
						lazyadapter = new LazyAdapter(context, mImage, mName);
						listView.setAdapter(lazyadapter);
						lazyadapter.notifyDataSetChanged();

						listView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1,
									int position, long arg3) {
								// TODO Auto-generated method stub
								//Toast.makeText(context, mName.get(position), Toast.LENGTH_SHORT).show();
								if(!isRefreshing){
									Intent intent=new Intent(context,WebViewActivity.class);
									intent.putExtra("tab_value", 3);
									intent.putExtra("movieYear", mYear.get(position-1));
									intent.putExtra("movieName", mName.get(position-1));
									intent.putExtra("movieImage", mImage.get(position-1));
									intent.putExtra("movieAwards", mAwards.get(position-1));
									intent.putExtra("movieDirector", mDirector.get(position-1));
									intent.putExtra("movieProducer", mProducer.get(position-1));
									intent.putExtra("movieActedAs", mActedAs.get(position-1));
									intent.putExtra("movieAnecdotes", mAnecdotes.get(position-1));
									intent.putExtra("movieSynopsis", mSynopsis.get(position-1));
									startActivity(intent);
									overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
								}
							}
						});
					}
					listView.onRefreshComplete();



				}catch (ParserConfigurationException e) {
					Log.e("Error: ", e.getMessage());

				} catch (SAXException e) {
					Log.e("Error: ", e.getMessage());

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				super.onPostExecute(result);
			}
		}

	}



	/*public static class CustomListAdapter extends ArrayAdapter<String>{

		Activity context;
		private ArrayList<String> row= new ArrayList<String>();
		private ArrayList<String> img=new ArrayList<String>();
		LayoutInflater inflator=null;
		static File cacheDir;
		ImageLoader imageLoader;

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.memoryCache(new WeakMemoryCache())
		.denyCacheImageMultipleSizesInMemory()
		.discCache(new UnlimitedDiscCache(cacheDir))
		.threadPoolSize(5)
		.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))
		.enableLogging()
		.build();


		DisplayImageOptions options = new DisplayImageOptions.Builder()
		.cacheOnDisc()
		.cacheInMemory()
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.displayer(new RoundedBitmapDisplayer(15))

		.build();



		public CustomListAdapter(Activity context,ArrayList<String> row,ArrayList<String> img)
		{
			super(context,R.layout.listrow,row);
			this.context=context;
			this.img.clear();
			this.row.clear();
			this.row=row;
			this.img=img;
			imageLoader =ImageLoader.getInstance();
			if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
				cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MadhuriMov");
			else
				cacheDir=context.getCacheDir();
			if(!cacheDir.exists())
				cacheDir.mkdirs();


		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView=convertView;
			if(convertView== null){

				inflator					=	context.getLayoutInflater();

				final ViewHolder holder		=	new ViewHolder();
				rowView						=	inflator.inflate(R.layout.listrow, null);
				holder.rowText				=	(TextView) rowView.findViewById(R.id.rowListTextView);
				holder.movieImage			= 	(ImageView) rowView.findViewById(R.id.movieImage);
				holder.layout 				=	(RelativeLayout) rowView.findViewById(R.id.text_relativelayout);
				holder.rowText.setText(row.get(position));
				imageloader.DisplayImage(shareUrl + img.get(position), holder.movieImage);
				rowView.setTag(R.id.rowListTextView, holder.rowText);
				rowView.setTag(R.id.text_relativelayout, holder.layout);
				rowView.setTag(holder);

			}


			ViewHolder hold=(ViewHolder)rowView.getTag();
			hold.rowText.setText(row.get(position));
			hold.movieImage.setBackgroundResource(new ImageFragment(context, shareUrl + img.get(position) ));
			imageloader.displayImage(shareUrl + img.get(position), hold.movieImage);
			imageloader.displayImage(shareUrl + img.get(position), hold.movieImage);



			return rowView;






			View vi=convertView;
			if(convertView==null){
				inflator = context.getLayoutInflater();
				vi = inflator.inflate(R.layout.listrow, null);
			}


			TextView text=(TextView)vi.findViewById(R.id.rowListTextView);
			ImageView image=(ImageView)vi.findViewById(R.id.movieImage);
			text.setText(row.get(position));
			imageloader.DisplayImage(shareUrl + img.get(position), image);
			return vi;
		}

		public int getCount() {
			return row.size();
		}



		public long getItemId(int position) {
			return position;
		}







	}


	static class ViewHolder
	{
		TextView rowText;
		ImageView movieImage;
		RelativeLayout layout;
	}*/


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*lazyadapter.clear();*/
		/*	if(isFinish){
			Intent intent = new Intent(context,MadhuriHomeScreen.class);
			startActivity(intent);
			finish();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}else{
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			super.onBackPressed();
		}

		 */

		if(showProgress.isShown() || isRefreshing){

			//final Dialog dialog = new Dialog(LAtestTab.this);
			/*dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
			dialog.setContentView(R.layout.alertbox);*/

			ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
			ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);
			dialog.show();

			yes.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					/*							lazyAdapter.clear();*/
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					if(isFinish){
						dialog.dismiss();
						LazyAdapter.imageloader.clearCache();
						if(lazyadapter!=null)
						{
							LazyAdapter.clear();
						}
						Intent intent = new Intent(context,MadhuriHomeScreen.class);
						startActivity(intent);
						/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
						downloadFile.cancel(true);
						finish();
					}else{
						dialog.dismiss();  
						LazyAdapter.imageloader.clearCache();
						if(lazyadapter!=null)
						{
							LazyAdapter.clear();
						}
						downloadFile.cancel(true);
						finish();
						/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
					}
				}
			});

			no.setOnClickListener(new OnClickListener()
			{


				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();  
				}
			});

			/*dialog.show();*/
		}
		else{
			/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
			LazyAdapter.imageloader.clearCache();
			if(lazyadapter!=null)
			{
				LazyAdapter.clear();
			}
			super.onBackPressed();
			/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
			if(fade)
			{
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
			}
			else
			{
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}
	}







}
