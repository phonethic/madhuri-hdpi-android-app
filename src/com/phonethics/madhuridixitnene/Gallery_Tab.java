package com.phonethics.madhuridixitnene;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.PushService;



public class Gallery_Tab extends FragmentActivity implements OnClickListener {

	static final String url 		= "http://madhuridixit-nene.com/uploads/gallery.xml";
	static final String KEY_ITEM 	= "photo"; // parent node
	String xml,jString;
	Document	doc;
	NodeList 	nl;
	DefaultHttpClient 	httpClient;
	HttpPost 			httpPost;
	HttpResponse		httpRes;
	HttpEntity			httpEnt;
	ArrayList<String> photoLink;
	ImageView pg;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	ImageView imgv;

	ViewPager mPager;
	Context context;
	File externalFile = new File(Environment.getExternalStorageDirectory(),"MadhuriGallery.xml");

	ImageView galBack,galForth;
	int										CACHE_VALIDITY;



	JSONObject								configJson,tempJson,jsonIntObj,jsonObjImg,jsonObjFull;
	JSONArray								categoryJson, attacmentJArr;




	/*	String xmlurl 		= "http://madhuridixit-nene.com/uploads/gallery.xml";*/
	/*String xmlurl 		= "http://madhuridixit-nene.com/wp/?json=get_page&dev=1&id=301";*/
	String xmlurl = "http://madhuridixit-nene.com/wp/?json=get_page&slug=gallery&exclude=content";
	ArrayList<String> mImageLink;
	Bundle bundle;
	boolean isFinish = true;
	boolean									fade;

	TextView headerTextInfo;
	public static RelativeLayout header,bck,galleryLayout;

	GestureDetector gdt= new GestureDetector(new GestureListener());



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery_tab);


		photoLink	=	new ArrayList<String>();
		pg=(ImageView)findViewById(R.id.prog);
		imgv=(ImageView)findViewById(R.id.imgv);
		mPager=(ViewPager)findViewById(R.id.vwp);


		header=(RelativeLayout)findViewById(R.id.header);
		bck=(RelativeLayout)findViewById(R.id.bck);
		galleryLayout=(RelativeLayout)findViewById(R.id.galleryLayout);
		galleryLayout.setOnClickListener(this);
		galBack=(ImageView)findViewById(R.id.galack);
		galForth=(ImageView)findViewById(R.id.galforth);
		headerTextInfo=(TextView)findViewById(R.id.headerTextInfo);

		CACHE_VALIDITY = Integer.parseInt(getResources().getString(R.string.CacheFileValidity));
		context=this;
		/*PushService.setDefaultPushCallback(context, Gallery_Tab.class);*/
		NetworkCheck netAvailable = new NetworkCheck(context);
		bundle = getIntent().getExtras();
		if(bundle != null){
			isFinish = bundle.getBoolean("finishAct");
			fade=bundle.getBoolean("shtcut");
		}
		if(netAvailable.isNetworkAvailable()){
			if(externalFile.exists()){
				Date lastModDate = new Date(externalFile.lastModified());
				Date todayDate = new Date();
				long fileDate = lastModDate.getTime();
				long currentDate = todayDate.getTime();
				long dateDiff = currentDate - fileDate;
				long diffDays = dateDiff / (24 * 60 * 60 * 1000);
				if(diffDays > CACHE_VALIDITY){
					Log.d("************", "-------NO OF DAYS DIFFERENCE-------" +diffDays);
					pg.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
					pg.setVisibility(View.VISIBLE);
					new FileFromURL().execute(xmlurl);
				}else{
					try{
						InputStream filename 	= null;
						Document xmldoc 		= null;
						DocumentBuilderFactory doc_build_fact 	= null;
						DocumentBuilder doc_builder = null;
						filename = new FileInputStream("/sdcard/MadhuriGallery.xml");
						if(externalFile.exists()){
							/*doc_build_fact = DocumentBuilderFactory.newInstance();

							doc_builder = doc_build_fact.newDocumentBuilder();
							xmldoc = doc_builder.parse(filename);
							NodeList nodeList = null;
							nodeList = xmldoc.getElementsByTagName("photo");
							for(int i=0; i<nodeList.getLength();i++){
								Node node = nodeList.item(i);
								Element fstElmnt = (Element) node;
								photoLink.add(fstElmnt.getAttribute("link"));
							}*/
							parseJson();

							mPager.setAdapter(new MyAdapter(getSupportFragmentManager(), context, photoLink));
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}else{
				pg.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				pg.setVisibility(View.VISIBLE);
				new FileFromURL().execute(xmlurl);
			}
		}else{
			//	Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			/*for(int i=0; i< 120; i++){
				photoLink.add("http://madhuridixit-nene.com/wp/wp-content/uploads/2012/04/w6-225x300.jpg");
			}*/

			try {
				if(externalFile.exists()){
					InputStream filename 	= null;
					Document xmldoc 		= null;
					DocumentBuilderFactory doc_build_fact 	= null;
					DocumentBuilder doc_builder = null;
					filename = new FileInputStream("/sdcard/MadhuriGallery.xml");
					if(externalFile.exists()){
						/*	doc_build_fact = DocumentBuilderFactory.newInstance();

						doc_builder = doc_build_fact.newDocumentBuilder();
						xmldoc = doc_builder.parse(filename);
						NodeList nodeList = null;
						nodeList = xmldoc.getElementsByTagName("photo");
						for(int i=0; i<nodeList.getLength();i++){
							Node node = nodeList.item(i);
							Element fstElmnt = (Element) node;
							photoLink.add(fstElmnt.getAttribute("link"));
						}
						mPager.setAdapter(new MyAdapter(getSupportFragmentManager(), context, photoLink));*/

						parseJson();
					}
				}else{
					Toast.makeText(context, "No Internet Connnection", Toast.LENGTH_SHORT).show();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




		}

		galBack.setOnClickListener(this);
		galForth.setOnClickListener(this);
		mPager.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (gdt.onTouchEvent(event)) {
					return false;
				}

				return false;
			}
		});
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			int dragPosition;
			ImageView img;
			boolean fade=true;
			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub
				dragPosition=position;

				if(mPager.getAdapter()!=null)
				{
					headerTextInfo.setText(""+(mPager.getCurrentItem()+1)+" of "+mPager.getAdapter().getCount());
				}

				img=(ImageView)mPager.findViewWithTag(dragPosition);
				if(img!=null)
				{
					img.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(fade)
							{
								Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
								Gallery_Tab.header.startAnimation(anim);
								Gallery_Tab.bck.startAnimation(anim);
								anim.setFillAfter(true);
								Gallery_Tab.header.setVisibility(ViewGroup.GONE);
								Gallery_Tab.bck.setVisibility(ViewGroup.GONE);
								fade=false;
							}
							else if(!fade)
							{
								Animation anim=AnimationUtils.loadAnimation(context, R.anim.iconfade_in);
								Gallery_Tab.header.setVisibility(ViewGroup.VISIBLE);
								Gallery_Tab.bck.setVisibility(ViewGroup.VISIBLE);
								Gallery_Tab.header.startAnimation(anim);
								Gallery_Tab.bck.startAnimation(anim);
								anim.setFillAfter(true);
								fade=true;
							}
						}
					});

				}


			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// TODO Auto-generated method stub

				if(state!=0)
				{

					/*if(img!=null)
					{
						img.setClickable(false);
					}*/
					/*	Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
					Gallery_Tab.header.startAnimation(anim);
					Gallery_Tab.bck.startAnimation(anim);
					anim.setFillAfter(true);
					Gallery_Tab.header.setVisibility(ViewGroup.GONE);
					Gallery_Tab.bck.setVisibility(ViewGroup.GONE);
					fade=false;*/
				}
				if(state==0)
				{

				}



			}
		});

		mPager.setOnClickListener(this);


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_gallery_tab, menu);
		return false;
	}


	class FileFromURL extends AsyncTask<String, String, String> {

		boolean errorCaught  = false;
		@Override
		protected String doInBackground(String... xmlur) {
			// TODO Auto-generated method stub

			int count;

			try{
				checkCurrentTime();
				HttpParams httpParams = new BasicHttpParams();

				//int timeoutConnection = 40000;
				//HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT) 
				// in milliseconds which is the timeout for waiting for data.
				//int timeoutSocket = 40000;
				//HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);


				httpClient 	=	new DefaultHttpClient(httpParams);
				httpPost 	= 	new HttpPost(xmlurl);

				httpRes		=	httpClient.execute(httpPost);
				httpEnt		=	httpRes.getEntity();
				xml 		=	EntityUtils.toString(httpEnt);

				URL url = new URL(xmlur[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				FileOutputStream output = new FileOutputStream("/sdcard/MadhuriGallery.xml");


				byte data[] = new byte[1024];

				long total = 0;
				while (   ((count = input.read(data)) != -1)  ) {
					total += count;
					Log.d("mdtotal", "MdGall Length >> "+total);
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress(""+(int)((total*100)/lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				output.flush();
				// closing streams
				output.close();
				input.close();



			}catch(SocketException socketException){
				Log.d("***", "Socket Exception");
				errorCaught = true;
			}catch(ConnectionClosedException con){
				errorCaught = true;
			}catch(ConnectTimeoutException timeOut){
				Log.d("***", "connection time out");
				errorCaught = true;
			}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception x){
				x.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			checkCurrentTime();
			if(errorCaught){
				Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
			}else{
				pg.clearAnimation();
				pg.setVisibility(View.GONE);
				parseJson();
			}
			super.onPostExecute(result);
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			checkCurrentTime();
		}




	}


	public void parseJson(){

		try{
			FileInputStream filename 				= null;
			filename = new FileInputStream("/sdcard/MadhuriGallery.xml");
			FileChannel fc = filename.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			jString = Charset.defaultCharset().decode(bb).toString();
			configJson = new JSONObject(jString);
			tempJson	=	configJson.getJSONObject("page");
			attacmentJArr = tempJson.getJSONArray("attachments");
			photoLink.clear();
			//Toast.makeText(context, "Attachments "+attacmentJArr.length(), 0).show();
			for(int i=0; i<attacmentJArr.length();i++){

				jsonIntObj = attacmentJArr.getJSONObject(i);
				jsonObjImg = jsonIntObj.getJSONObject("images");
				jsonObjFull = null;
				try{
					jsonObjFull = jsonObjImg.getJSONObject("full");
				}catch(Exception ex){
					ex.printStackTrace();
				}
				if(jsonObjFull ==null){
					jsonObjFull = jsonObjImg.getJSONObject("medium");
				}
				String thisUrl = jsonObjFull.getString("url");
				photoLink.add(thisUrl);
			}
		}catch(JSONException e)        {
			Log.e("log_tag", "Error parsing data "+e.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e){
			Log.e("Error: ", " "+e.getMessage());
		}

		for(int i=0; i<photoLink.size();i++)
		{
			Log.d("====>>>", "Link " + i + " " + photoLink.get(i));
		}
		mPager.setAdapter(new MyAdapter(getSupportFragmentManager(), context, photoLink));


	}

	public  class MyAdapter extends FragmentPagerAdapter
	{

		Context context;
		ArrayList<String> url=null;
		public MyAdapter(FragmentManager fm,Context context,ArrayList<String>url) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.url=url;
			/*for(int i=0;i<url.size();i++)
			{
				Log.i("Link:"," "+url.get(i));
			}*/
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
			return new ImageFragment(context, url.get(position),position);

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return url.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}



	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		try{
			if((externalFile.exists()) && (externalFile.length() < 1024)){
				externalFile.delete();
			}}catch(Exception exp){
				exp.printStackTrace();
			}
		/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
		if(isFinish){
			Intent intent = new Intent(context,MadhuriHomeScreen.class);
			startActivity(intent);

			finish();

			//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);

			if(fade)
			{
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
			}
			else
			{
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}

		}else{

			super.onBackPressed();
			//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			if(fade)
			{
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
			}
			else
			{
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}

		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if(v.getId()==galBack.getId())
		{
			int currentIMage=mPager.getCurrentItem();
			if(mPager.getAdapter()!=null)
			{
				if(currentIMage!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(currentIMage-1, true);
					headerTextInfo.setText(""+(mPager.getCurrentItem()+1)+" of "+mPager.getAdapter().getCount());
				}
			}
		}
		if(v.getId()==galForth.getId())
		{
			int currentIMage=mPager.getCurrentItem();
			if(mPager.getAdapter()!=null)
			{
				if(currentIMage!=mPager.getAdapter().getCount())
				{
					mPager.setCurrentItem(currentIMage+1, true);
					headerTextInfo.setText(""+(mPager.getCurrentItem()+1)+" of "+mPager.getAdapter().getCount());
				}
			}

		}



	}



	private class GestureListener extends SimpleOnGestureListener
	{

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			// TODO Auto-generated method stub
			/*	Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
			Gallery_Tab.header.startAnimation(anim);
			Gallery_Tab.bck.startAnimation(anim);
			anim.setFillAfter(true);
			Gallery_Tab.header.setVisibility(ViewGroup.GONE);
			Gallery_Tab.bck.setVisibility(ViewGroup.GONE);
			return super.onScroll(e1, e2, distanceX, distanceY);*/
			return super.onScroll(e1, e2, distanceX, distanceY);
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			/*if(fade)
			{
				Animation anim=AnimationUtils.loadAnimation(context, R.anim.fade_out);
				Gallery_Tab.header.startAnimation(anim);
				Gallery_Tab.bck.startAnimation(anim);
				anim.setFillAfter(true);
				Gallery_Tab.header.setVisibility(ViewGroup.GONE);
				Gallery_Tab.bck.setVisibility(ViewGroup.GONE);
				fade=false;
			}
			else if(!fade)
			{
				Animation anim=AnimationUtils.loadAnimation(context, R.anim.iconfade_in);
				Gallery_Tab.header.setVisibility(ViewGroup.VISIBLE);
				Gallery_Tab.bck.setVisibility(ViewGroup.VISIBLE);
				Gallery_Tab.header.startAnimation(anim);
				Gallery_Tab.bck.startAnimation(anim);
				anim.setFillAfter(true);
				fade=true;
			}*/
			return super.onSingleTapUp(e);
		}

	}



	public void checkCurrentTime(){
		Calendar cal = Calendar.getInstance();
		long time = cal.getTimeInMillis();
		Log.d("", "MdGall Current Time >> " +time);
	}




}

