package com.phonethics.madhuridixitnene;



import com.parse.PushService;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class FbTwtrLink extends Activity {

	WebView web;
	ImageView img,bckimg,share_btn,bck,forth,refresh;	
	ImageView showProgress;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_fb_twtr_link);
		context = this;
	/*	PushService.setDefaultPushCallback(context, FbTwtrLink.class);*/

		Bundle bundle=getIntent().getExtras();

		img = (ImageView)findViewById(R.id.header);
		bckimg = (ImageView) findViewById(R.id.wbview_bckbtn);
		bck = (ImageView) findViewById(R.id.webback);
		forth = (ImageView) findViewById(R.id.forth);
		refresh = (ImageView) findViewById(R.id.refresh);

		showProgress=(ImageView)findViewById(R.id.showProgress);

		web=(WebView)findViewById(R.id.linkwebview);
		web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		String userAgent1 = "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";
		String userAgent = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
		//String userAgent = "Mozilla/5.0 (Linux; U; Android 2.0; en-US; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Firefox/4.0";
		/*web.getSettings().setUserAgentString(userAgent);
		web.getSettings().setBuiltInZoomControls(true);
		
		web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		web.clearHistory();
		web.getSettings().setDomStorageEnabled(false);*/
		
	

		int fb_link = bundle.getInt("fb_link");
		int site_link= bundle.getInt("sitelink");

		if(fb_link == 1){

			img.setImageResource(R.drawable.bar_facebook);
			img.setVisibility(View.VISIBLE);
			bckimg.setVisibility(View.VISIBLE);
			//web.getSettings().setUserAgentString(userAgent1);
			web.getSettings().setBuiltInZoomControls(true);
			
			web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			web.clearHistory();
			web.getSettings().setDomStorageEnabled(false);
			web.setWebViewClient(new MyWebViewClient());
			web.loadUrl("https://www.facebook.com/MadhuriDixitNene");
			//web.loadUrl("https://m.facebook.com/MadhuriDixitNene");
			

		}


		else if(fb_link == 2)
		{
			img.setImageResource(R.drawable.bar_twitter);
			img.setVisibility(View.VISIBLE);
			bckimg.setVisibility(View.VISIBLE);
			web.getSettings().setUserAgentString(userAgent);
			web.getSettings().setBuiltInZoomControls(true);
			
			web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			web.clearHistory();
			web.getSettings().setDomStorageEnabled(false);
			web.setWebViewClient(new MyWebViewClient());
			//web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			//web.loadUrl("https://www.twitter.com/MadhuriDixit/");
			web.loadUrl("https://mobile.twitter.com/madhuridixit");

		}

		if(site_link==3)
		{
			img.setVisibility(View.GONE);
			bckimg.setVisibility(View.GONE);
			web.getSettings().setUserAgentString(userAgent);
			web.getSettings().setBuiltInZoomControls(true);
			
			web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			web.clearHistory();
			web.getSettings().setDomStorageEnabled(false);
			web.setWebViewClient(new MyWebViewClient());
			
			web.loadUrl("http://phonethics.in/");
		}


		bckimg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_fb_twtr_link, menu);
		return false;
	}

	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		
		
		
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			
			view.loadUrl(url);
			Log.d("", "MdLoadURl >> " +url);

		/*	if(!web.canGoBack())
			{
				bck.setEnabled(false);
				bck.setImageResource(R.drawable.arrowleft_inactive);
			}
			else if(web.canGoBack())
			{
				bck.setEnabled(true);
				bck.setImageResource(R.drawable.arrowleft_active);
			}
			if(web.canGoForward())
			{
				forth.setEnabled(true);
				forth.setImageResource(R.drawable.arrowright_active);
			}
			else if(!web.canGoForward())
			{
				forth.setEnabled(false);
				forth.setImageResource(R.drawable.arrowright_inactive);
			}*/

		
				bck.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(web.canGoBack())
						{
						web.goBack();
						}
					}
				});
		
				forth.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(web.canGoForward())
						{
						web.goForward();
						}


					}
				});
		

			refresh.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					web.reload();

				}
			});



			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			showProgress.clearAnimation();
			showProgress.setVisibility(View.GONE);
		}
		

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);

			showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
			showProgress.setVisibility(View.VISIBLE);
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);

		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
	}

	
	

}
