package com.phonethics.madhuridixitnene;

import java.io.File;
import java.util.ArrayList;



import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/*import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;*/

public class LazyAdapter extends BaseAdapter {

	private static Activity activity;
	private ArrayList<String> movieThumbnail = new ArrayList<String>();
	private ArrayList<String> movieText = new ArrayList<String>();
	private static LayoutInflater inflater=null;
	static File cacheDir;
	Typeface tf;
	/*LatestTab tab = new LatestTab();*/

	/*ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity)
	.memoryCache(new WeakMemoryCache())
	.denyCacheImageMultipleSizesInMemory()
	.discCache(new UnlimitedDiscCache(cacheDir))
	.threadPoolSize(2)
	.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) 
	.memoryCacheExtraOptions(40, 40)
	.imageDownloader(new URLConnectionImageDownloader(120 * 1000, 120 * 1000))
	.enableLogging()

	.build();

    DisplayImageOptions options = new DisplayImageOptions.Builder()
	.cacheOnDisc()
	.cacheInMemory()
	.bitmapConfig(Bitmap.Config.RGB_565)
	.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
	.build();
	 */
	/*    private ImageLoader imageLoader= ImageLoader.getInstance();*/
	static ImageLoader imageloader;

	public LazyAdapter(Activity a){
		imageloader = new ImageLoader(a);
	}
	
	
	public LazyAdapter(Activity a, ArrayList<String> movieThumbnail, ArrayList<String> movieText) {
		/*imageloader = new ImageLoader(activity);*/
		this.movieThumbnail.clear();
		this.movieText.clear();
		activity = a;
		imageloader = new ImageLoader(activity);
		/*data=d;*/
		this.movieThumbnail = movieThumbnail;
		this.movieText = movieText;
		tf=Typeface.createFromAsset(activity.getAssets(), "fonts/cochinbold.ttf");


		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		/* if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"Madhuri/cache");
		else
			cacheDir=activity.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();*/

		/* imageLoader.init(config);*/




	}

	public int getCount() {
		return movieText.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
		if(convertView==null)
			vi = inflater.inflate(R.layout.listrow, null);

		TextView text=(TextView)vi.findViewById(R.id.rowListTextView); 
		text.setTypeface(tf);
		//text.setShadowLayer(1, 2, 0, Color.WHITE);
		ImageView image=(ImageView)vi.findViewById(R.id.movieImage);
		text.setText(Html.fromHtml(movieText.get(position)));
		
		imageloader.DisplayImage(movieThumbnail.get(position), image);

		return vi;


		/*if(count == 0){
			TextView text=(TextView)vi.findViewById(R.id.rowListTextView);
			ImageView image=(ImageView)vi.findViewById(R.id.movieImage);
			text.setText(movieText.get(position));
			imageloader.DisplayImage(movieThumbnail.get(position), image);
			imageLoader.displayImage(movieThumbnail.get(position), image, options);

		}else if(count == 1){
			TextView text=(TextView)vi.findViewById(R.id.rowListTextView);
			ImageView image=(ImageView)vi.findViewById(R.id.movieImage);
			if(position==0){
				text.setText(movieText.get(position));
				imageloader.DisplayImage(movieThumbnail.get(position), image);
			}else{
				text.setText(movieText.get(position-1));
				imageloader.DisplayImage(movieThumbnail.get(position-1), image);
			}

			if(position<movieThumbnail.size()){
				text.setText(movieText.get(position));
				imageloader.DisplayImage(movieThumbnail.get(position), image);
			}


		}
		return vi;*/
	}

	public static void clear(){
		imageloader.clearCache();
	}
}