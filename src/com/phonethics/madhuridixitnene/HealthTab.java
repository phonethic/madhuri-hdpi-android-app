package com.phonethics.madhuridixitnene;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.PushService;




public class HealthTab extends Activity {

	Activity context;
	ArrayList<String> contentList,slugList,healthData, imgUrl;
	ArrayList<HashMap<String, String>> mylist;
	JSONObject json, img,obj,thumb;
	JSONArray  health,attachment;
	ImageView bck;
	String shareUrl ="http://madhuridixit-nene.com/health-beauty/post/";
	String healthUrl = "http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=health-beauty&count=-1";
	String jString,imgStr;
	ImageView showProgress;
	NetworkCheck netCheck;
	//File externalFile 		= new File(Environment.getExternalStorageDirectory(),"MadhuriHealth.txt");
	PullToRefreshListView listView;

	int mb = 1024*1024;
	ImageLoader imageLoader;
	LazyAdapter lazyAdapter;
	Bundle bundle;
	boolean isFinish = true, errorCaught = false,isRefreshing = false;
	int p=0;
	Dialog dialog;
	int									presentSize=0, newSize,dataSize=0;
	boolean									fade;
	MyAsyncTask								downloadFile;
	int										CACHE_VALIDITY;




	JSONObject								objJson,configJson,tempJson;
	JSONArray								categoryJson;
	ImageView 								btnLoadMore;
	ObjectSerializer						objSerial;
	ListViewContent							listContetnt;
	PostCount								postCount;
	TabConfig								tabAsync;
	String 									healthUrlWithCount = "http://madhuridixit-nene.com/wp/?json=get_category_posts&dev=1&slug=health-beauty&page=";
	String									TAB_INFO ="/mnt/sdcard/MdNTabpostConfig.bin";
	String									TABINFO_URL="http://madhuridixit-nene.com/wp/?json=get_category_index&dev=1";
	File 									serialFile = new File(Environment.getExternalStorageDirectory(),"MdNHealth.bin");
	File									configFile= new File(Environment.getExternalStorageDirectory(),"MdNTabpostConfig.bin");
	int										pageCount=1,totalPages,TOTAL_POST_COUNT;
	ArrayList<Integer> 						postID;
	ArrayList<ListViewContent> 				listObject;
	ArrayList<PostCount> 					postConfigArrray;
	String									MDHEALTH = "/mnt/sdcard/MdNHealth.bin";
	String									THIS_TAB = "health-beauty";



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_health_tab);

		context=this;
		/*PushService.setDefaultPushCallback(context, HealthTab.class);*/
		bck = (ImageView) findViewById(R.id.bckbtn);
		showProgress=(ImageView)findViewById(R.id.showProgress);
		netCheck 	= 		new NetworkCheck(context);
		mylist 		= 		new ArrayList<HashMap<String, String>>();
		contentList	=		new ArrayList<String>();
		slugList	=		new ArrayList<String>();
		healthData	=		new ArrayList<String>();
		imgUrl	=		new ArrayList<String>();
		lazyAdapter			= new LazyAdapter(context);
		listView	= (PullToRefreshListView) findViewById(R.id.healthlist);
		dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertbox);
		listView.setVisibility(View.GONE);

		mylist 				= new ArrayList<HashMap<String, String>>();
		contentList			= new ArrayList<String>();
		slugList			= new ArrayList<String>();
		lazyAdapter			= new LazyAdapter(context);

		objSerial	=		new ObjectSerializer();
		btnLoadMore = 		(ImageView) findViewById(R.id.btnLoadMore);
		postID		=		new ArrayList<Integer>();
		listObject			= new ArrayList<ListViewContent>();
		postConfigArrray	= new  ArrayList<PostCount>();
		tabAsync			= new TabConfig();

		CACHE_VALIDITY = Integer.parseInt(getResources().getString(R.string.CacheFileValidity));

		downloadFile = new MyAsyncTask();

		bundle = getIntent().getExtras();
		if(bundle != null){
			isFinish = bundle.getBoolean("finishAct");

			fade=bundle.getBoolean("shtcut");
		}


		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		listView.setLayoutAnimation(controller);


		if(netCheck.isNetworkAvailable()){

			/*Date lastModDate = new Date(externalFile.lastModified());
				Date todayDate = new Date();
				long fileDate = lastModDate.getTime();
				long currentDate = todayDate.getTime();
				long dateDiff = currentDate - fileDate;
				long diffDays = dateDiff / (24 * 60 * 60 * 1000);
				if(diffDays > CACHE_VALIDITY){
					Log.d("************", "-------NO OF DAYS DIFFERENCE-------" +diffDays);
					showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
					showProgress.setVisibility(View.VISIBLE);
					downloadFile.execute(healthUrl);
				}else{
					try {
						FileInputStream filename 				= null;
						Document xmldoc	 						= null;
						DocumentBuilderFactory doc_build_fact 	= null;
						DocumentBuilder doc_builder 			= null;
						filename = new FileInputStream("/sdcard/MadhuriHealth.txt");



						FileChannel fc = filename.getChannel();
						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						jString = Charset.defaultCharset().decode(bb).toString();
						json = new JSONObject(jString); 
						objJson = json.getJSONObject("category");
						dataSize = objJson.getInt("post_count");
						health = json.getJSONArray("posts");
						dataSize = health.length();
						for(int i=0;i<health.length();i++){	

							HashMap<String, String> map = new HashMap<String, String>();	
							JSONObject e = health.getJSONObject(i);

							String name = e.getString("title_plain");
							String content = e.getString("content");
							contentList.add(content);
							if(e.has("thumbnail")){
								imgStr = e.getString("thumbnail");
							}else{
								imgStr = "http://madhuridixit-nene.com//wp//wp-content//uploads//2012//09//final2.jpg";
							}
							attachment = e.getJSONArray("attachments");
							for(int p=0; p<attachment.length();p++){
								obj = attachment.getJSONObject(p);
								img = obj.getJSONObject("images");
								thumb = img.getJSONObject("thumbnail");
								imgStr = thumb.getString("url");
							}	

							String chng = "&#8211;";

							String 	fnl_Str = name.replace(chng, "");
							String slug = e.getString("slug");
							slugList.add(slug);

							map.put("id",  String.valueOf(i));
							map.put("name", fnl_Str);
							map.put("date", e.getString("date"));

							healthData.add(fnl_Str);
							imgUrl.add(imgStr);



							mylist.add(map);			
						}		
					}catch(JSONException e)        {
						Log.e("log_tag", "Error parsing data "+e.toString());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch (Exception e){
						Log.e("Error: ", " "+e.getMessage());
					}


					showProgress.clearAnimation();
					showProgress.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
					lazyAdapter = new LazyAdapter(context, imgUrl, healthData);
					listView.setAdapter(lazyAdapter);
					lazyAdapter.notifyDataSetChanged();
				}
			}else{
				showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				showProgress.setVisibility(View.VISIBLE);
				downloadFile.execute(healthUrl);
			}*/



			if(!configFile.exists() || configFile.length()==0){
				/*
				 *  If configFile Dosent exist or file length is zero then serialize it.
				 */
				tabAsync.execute(TABINFO_URL);
			}else{
				/*
				 * Deserializing  and store latest post count
				 */
				Object deserialTabInfo =  objSerial.deserialize(TAB_INFO);
				postConfigArrray = (ArrayList<PostCount>)deserialTabInfo;
				for(int i=0;i<postConfigArrray.size();i++){
					if(postConfigArrray.get(i).getTabName().equalsIgnoreCase(THIS_TAB)){
						TOTAL_POST_COUNT = postConfigArrray.get(i).getPostCount();

					}
				}

				Log.i("*******", "Total POst " + TOTAL_POST_COUNT);
			}

			if(serialFile.exists()){

				/*
				 *  if network is available and file exist deserialize it 
				 */

				Object	deserial = objSerial.deserialize(MDHEALTH);
				listObject=(ArrayList<ListViewContent>)deserial;
				for(int r=0; r<listObject.size();r++){
					imgUrl.add(listObject.get(r).getThumbnailLink());
					healthData.add(listObject.get(r).getTitle());
					slugList.add(listObject.get(r).getSlug());
					contentList.add(listObject.get(r).getContentWebLink());
					pageCount = listObject.get(r).getPageState();
					postID.add(listObject.get(r).getPostId());
				}

				lazyAdapter = new LazyAdapter(context, imgUrl, healthData);
				listView.setAdapter(lazyAdapter);
				listView.setVisibility(View.VISIBLE);
				lazyAdapter.notifyDataSetChanged();
				if(healthData.size()< TOTAL_POST_COUNT){
					btnLoadMore.setVisibility(View.VISIBLE);
				}
				Log.i("******0---*", "lateset Data " + healthData.size());
			}else{
				/*
				 *  net connected but no such file exist then download content
				 */


				Log.d("************", "-------EXTERNAL FILOE WAS NOT THERE DOWNLOADING NEW ONE-------");
				showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				showProgress.setVisibility(View.VISIBLE);
				downloadFile.execute(healthUrlWithCount+pageCount);
			}
		}else{

			/*	//	Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			if(externalFile.exists()){
				try {
					FileInputStream filename 				= null;
					Document xmldoc	 						= null;
					DocumentBuilderFactory doc_build_fact 	= null;
					DocumentBuilder doc_builder 			= null;
					filename = new FileInputStream("/sdcard/MadhuriHealth.txt");



					FileChannel fc = filename.getChannel();
					MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
					jString = Charset.defaultCharset().decode(bb).toString();
					json = new JSONObject(jString); 
					objJson = json.getJSONObject("category");
					dataSize = objJson.getInt("post_count");
					health = json.getJSONArray("posts");
					dataSize = health.length();
					for(int i=0;i<health.length();i++){	

						HashMap<String, String> map = new HashMap<String, String>();	
						JSONObject e = health.getJSONObject(i);

						String name = e.getString("title_plain");
						String content = e.getString("content");
						contentList.add(content);
						if(e.has("thumbnail")){
							imgStr = e.getString("thumbnail");
						}else{
							imgStr = "http://madhuridixit-nene.com//wp//wp-content//uploads//2012//09//final2.jpg";
						}
						attachment = e.getJSONArray("attachments");
						for(int p=0; p<attachment.length();p++){
							obj = attachment.getJSONObject(p);
							img = obj.getJSONObject("images");
							thumb = img.getJSONObject("thumbnail");
							imgStr = thumb.getString("url");
						}	

						String chng = "&#8211;";

						String 	fnl_Str = name.replace(chng, "");
						String slug = e.getString("slug");
						slugList.add(slug);

						map.put("id",  String.valueOf(i));
						map.put("name", fnl_Str);
						map.put("date", e.getString("date"));

						healthData.add(fnl_Str);
						imgUrl.add(imgStr);



						mylist.add(map);			
					}		
				}catch(JSONException e)        {
					Log.e("log_tag", "Error parsing data "+e.toString());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch (Exception e){
					Log.e("Error: ", " "+e.getMessage());
				}


				showProgress.clearAnimation();
				showProgress.setVisibility(View.GONE);
				listView.setVisibility(View.VISIBLE);
				lazyAdapter = new LazyAdapter(context, imgUrl, healthData);
				listView.setAdapter(lazyAdapter);
				lazyAdapter.notifyDataSetChanged();


			}
			else{
				Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			}*/


			/*
			 * 
			 *  If net is not connected and file exist
			 */
			if(serialFile.exists()){
				Object	deserial = objSerial.deserialize(MDHEALTH);
				listObject=(ArrayList<ListViewContent>)deserial;
				for(int r=0; r<listObject.size();r++){
					imgUrl.add(listObject.get(r).getThumbnailLink());
					healthData.add(listObject.get(r).getTitle());
					slugList.add(listObject.get(r).getSlug());
					contentList.add(listObject.get(r).getContentWebLink());
					pageCount = listObject.get(r).getPageState();
					postID.add(listObject.get(r).getPostId());
				}
				lazyAdapter = new LazyAdapter(context, imgUrl, healthData);
				listView.setAdapter(lazyAdapter);
				listView.setVisibility(View.VISIBLE);
				lazyAdapter.notifyDataSetChanged();

			}else{

				Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			}

		}

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		

				if(!isRefreshing){
					Intent intent=new Intent(context,WebViewActivity.class);
					intent.putExtra("content", contentList.get(position-1));
					intent.putExtra("shareUrl", shareUrl+slugList.get(position-1));
					intent.putExtra("tab_value", 5);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});

		/*
		 * Loading More records 
		 */
		btnLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(   (!isRefreshing) && (!showProgress.isShown())  ){
					if(pageCount < totalPages || totalPages == 0){
						pageCount += 1;

						showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
						showProgress.setVisibility(View.VISIBLE);
						new MyAsyncTask().execute(healthUrlWithCount+pageCount);
					}
				}
			}
		});

		bck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(showProgress.isShown() || isRefreshing){

					/*//final Dialog dialog = new Dialog(LAtestTab.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
					dialog.setContentView(R.layout.alertbox);*/

					ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
					ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);

					yes.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v) 
						{
							/*		lazyAdapter.clear();*/
							if(listObject.size()!=0){
								objSerial.serialize((Object)listObject,MDHEALTH);
							}
							/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
							if(isFinish){
								dialog.dismiss();
								LazyAdapter.imageloader.clearCache();
								if(lazyAdapter!=null)
								{
									LazyAdapter.clear();
								}
								Intent intent = new Intent(context,MadhuriHomeScreen.class);
								startActivity(intent);
								//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}
								downloadFile.cancel(true);
								finish();
							}else{
								dialog.dismiss();
								LazyAdapter.imageloader.clearCache();
								if(lazyAdapter!=null)
								{
									LazyAdapter.clear();
								}
								downloadFile.cancel(true);
								finish();
								//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}
							} 
						}
					});

					no.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v) 
						{
							dialog.dismiss();  

						}
					});

					dialog.show();
				}
				else{ 
					if(listObject.size()!=0){
						objSerial.serialize((Object)listObject,MDHEALTH);
					}
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					LazyAdapter.imageloader.clearCache();
					if(lazyAdapter!=null)
					{
						LazyAdapter.clear();
					}
					downloadFile.cancel(true);
					finish();
					//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


					if(fade)
					{
						overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					}
					else
					{
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}
			}
		});


		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				/*	refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);*/

				// Do work to refresh the list here.
				if(netCheck.isNetworkAvailable()){
					isRefreshing = true;

					/*try {
						FileInputStream filename 				= null;
						Document xmldoc	 						= null;
						DocumentBuilderFactory doc_build_fact 	= null;
						DocumentBuilder doc_builder 			= null;
						filename = new FileInputStream("/sdcard/MadhuriHealth.txt");

						FileChannel fc = filename.getChannel();
						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						jString = Charset.defaultCharset().decode(bb).toString();
						json = new JSONObject(jString); 
						health = json.getJSONArray("posts");
						presentSize = health.length();
						objJson = json.getJSONObject("category");
						presentSize = objJson.getInt("post_count");

					}catch(JSONException e){
						Log.e("log_tag", "Error parsing data "+e.toString());

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					catch (Exception e){
						Log.e("Error: ", " "+e.getMessage());
					}

					new MyAsyncTask().execute(healthUrl);*/


					if(!showProgress.isShown()){
						new TabConfig().execute(TABINFO_URL);
					}else{
						listView.onRefreshComplete();
					}

				}else{
					listView.onRefreshComplete();
				}
			}
		});
	}

	class MyAsyncTask extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>> > {

		boolean exceptionOccured  = false;

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {
			// TODO Auto-generated method stub

			int count;

			InputStream is = null;
			String result = "";
			JSONObject jObj = null;


			try{


				try{
					/*DefaultHttpClient  httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(params[0]);*/

					HttpGet httpGet = new HttpGet(params[0]);
					HttpParams httpParams = new BasicHttpParams();

					int timeoutConnection = 30000;
					HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 30000;
					HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

					DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
					HttpResponse response = httpClient.execute(httpGet);

					HttpEntity entity = response.getEntity();
					is = entity.getContent();

				}catch(Exception ex){
					Log.e("log_tag", "Error in http connection "+ex.toString());
					ex.printStackTrace();
					exceptionOccured = true;
				}

				try{
					if(!exceptionOccured){
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						StringBuilder	sb  = new StringBuilder();
						String line = null;

						while ((line = reader.readLine())!=null) {
							sb.append(line + "\n");

						}
						is.close();
						result = sb.toString();
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error converting result "+ex.toString());
					exceptionOccured = true;
				}

				//try parse the string to a JSON object
				try{
					if(!exceptionOccured){
						jObj = new JSONObject(result);
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error parsing data "+ex.toString());
					exceptionOccured = true;
				}


				if(!exceptionOccured){

					//json = JSONfunctions.getJSONfromURL(params[0]);

					objJson = jObj.getJSONObject("category");
					dataSize = objJson.getInt("post_count");
					totalPages = jObj.getInt("pages");




					health = jObj.getJSONArray("posts");
					if(isRefreshing && pageCount == 1){
						contentList.clear();
						slugList.clear();
						healthData.clear();
						imgUrl.clear();
						postID.clear();
						listObject.clear();
					}
					dataSize = health.length();
					for(int i=0;i<health.length();i++){	

						HashMap<String, String> map = new HashMap<String, String>();	
						JSONObject e = health.getJSONObject(i);

						String name = e.getString("title_plain");

						String content = e.getString("content");
						int postId = e.getInt("id");


						attachment = e.getJSONArray("attachments");

						for(int p=0; p<attachment.length();p++){
							/*obj = attachment.getJSONObject(p);
						img = obj.getJSONObject("images");
						thumb = img.getJSONObject("thumbnail");
						imgStr = thumb.getString("url");*/


							obj = attachment.getJSONObject(p);
							img = null;
							try{
								img = obj.getJSONObject("images");
							}catch(Exception ex){
								ex.printStackTrace();
							}
							if(img!=null){
								thumb= null;
								try{
									thumb = img.getJSONObject("thumbnail");
								}catch(Exception ex){
									ex.printStackTrace();
								}
								if(thumb!=null){
									imgStr = thumb.getString("url");	
								}else{
									imgStr="";
								}	
							}else{
								imgStr="";	
							}
						}	
						String 	fnl_Str="";
						String slug = "";

						if((postID.size()== 0) || (!postID.contains(postId))){
							contentList.add(content);
							String chng = "&#8211;";
							String 	sub_fnl_Str = name.replace(chng, "");
							String chng1 = "&#8230;";
							fnl_Str = sub_fnl_Str.replace(chng1, "");
							slug = e.getString("slug");
							slugList.add(slug);

							healthData.add(fnl_Str);
							imgUrl.add(imgStr);

							postID.add(postId);
							listContetnt		= new ListViewContent();
							listContetnt.setContentWebLink(content);
							listContetnt.setPostId(postId);
							listContetnt.setThumbnailLink(imgStr);
							listContetnt.setTitle(fnl_Str);
							listContetnt.setSlug(slug);
							listContetnt.setShareUrl(shareUrl);
							listContetnt.setPageState(pageCount);
							listObject.add(listContetnt);
						}

						map.put("id",  String.valueOf(i));
						map.put("name", fnl_Str);
						map.put("date", e.getString("date"));
						mylist.add(map);			
					}
				}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}



			return mylist;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {

			if(healthData.size() < TOTAL_POST_COUNT){
				btnLoadMore.setVisibility(View.VISIBLE);
			}else{
				btnLoadMore.setVisibility(View.GONE);
			}
			btnLoadMore.setClickable(true);
			showProgress.clearAnimation();
			showProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			isRefreshing = false;
			if(dialog.isShowing()){
				dialog.dismiss();
			}

			if(errorCaught){
				Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
			}else{
				lazyAdapter=null;
				lazyAdapter = new LazyAdapter(context, imgUrl, healthData);
				listView.setAdapter(lazyAdapter);
				lazyAdapter.notifyDataSetChanged();
			}


			if(pageCount == totalPages){
				btnLoadMore.setVisibility(View.GONE);
			}else{
				btnLoadMore.setVisibility(View.VISIBLE);
			}



			listView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		

					if(!isRefreshing){
						Intent intent=new Intent(context,WebViewActivity.class);
						intent.putExtra("content", contentList.get(position-1));
						intent.putExtra("shareUrl", shareUrl+slugList.get(position-1));
						intent.putExtra("tab_value", 5);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
					}
				}
			});


			listView.onRefreshComplete();



		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			listView.setOnItemClickListener(null);
			btnLoadMore.setClickable(false);
			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub

			super.onProgressUpdate(values);

		}
	}
	class TabConfig extends AsyncTask<String, Integer, ArrayList<HashMap<String, Integer>> >{

		boolean exceptionOccured  = false;

		@Override
		protected ArrayList<HashMap<String, Integer>> doInBackground(
				String... params) {


			InputStream is = null;
			String result = "";
			JSONObject jObj = null;



			// TODO Auto-generated method stub
			try {


				try{
					/*DefaultHttpClient  httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(params[0]);*/

					HttpGet httpGet = new HttpGet(params[0]);
					HttpParams httpParams = new BasicHttpParams();

					int timeoutConnection = 30000;
					HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 30000;
					HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

					DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
					HttpResponse response = httpClient.execute(httpGet);

					HttpEntity entity = response.getEntity();
					is = entity.getContent();

				}catch(Exception ex){
					Log.e("log_tag", "Error in http connection "+ex.toString());
					ex.printStackTrace();
					exceptionOccured = true;
				}



				//convert response to string
				try{
					if(!exceptionOccured){
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						StringBuilder	sb  = new StringBuilder();
						String line = null;

						while ((line = reader.readLine())!=null) {
							sb.append(line + "\n");

						}
						is.close();
						result = sb.toString();
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error converting result "+ex.toString());
					exceptionOccured = true;
				}

				//try parse the string to a JSON object
				try{
					if(!exceptionOccured){
						jObj = new JSONObject(result);
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error parsing data "+ex.toString());
					exceptionOccured = true;
				}


				if(!exceptionOccured){


					//configJson 	= JSONfunctions.getJSONfromURL(params[0]);
					categoryJson	=	jObj.getJSONArray("categories");
					for(int i=0; i <categoryJson.length(); i++){
						tempJson = categoryJson.getJSONObject(i);
						String title = tempJson.getString("slug");

						int post_count = tempJson.getInt("post_count");
						if(title.equalsIgnoreCase(THIS_TAB)){
							TOTAL_POST_COUNT = post_count;
						}
						Log.i("*** Asynch **", "Total Post " + TOTAL_POST_COUNT);
						postCount = new PostCount();
						postCount.setPostCount(post_count);
						postCount.setTabName(title);
						postConfigArrray.add(postCount);
					}
				}
			} catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, Integer>> result) {
			// TODO Auto-generated method stub

			objSerial.serialize((Object)postConfigArrray, TAB_INFO);
			for(int i=0;i<postConfigArrray.size();i++){
				if(postConfigArrray.get(i).getTabName().equalsIgnoreCase(THIS_TAB)){
					TOTAL_POST_COUNT = postConfigArrray.get(i).getPostCount();

				}
			}
			/*if(healthData.size()< TOTAL_POST_COUNT){
				btnLoadMore.setVisibility(View.VISIBLE);
				if(isRefreshing){
					new MyAsyncTask().execute(healthUrlWithCount+pageCount);
				}
			}*/
			if(isRefreshing){
				pageCount  = 1;
				new MyAsyncTask().execute(healthUrlWithCount+pageCount);
			}
			else{
				btnLoadMore.setVisibility(View.GONE);
				listView.onRefreshComplete();
				isRefreshing = false;
				if(dialog.isShowing()){
					dialog.dismiss();
				}
			}
			super.onPostExecute(result);
		}


	}







	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_health_tab, menu);
		return false;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*	lazyAdapter.clear();*/
		/*if(isFinish){
			Intent intent = new Intent(context,MadhuriHomeScreen.class);
			startActivity(intent);

			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			finish();

		}else{

			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			super.onBackPressed();
		}*/


		if(showProgress.isShown() || isRefreshing){

			/*//final Dialog dialog = new Dialog(LAtestTab.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
			dialog.setContentView(R.layout.alertbox);*/

			ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
			ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);
			dialog.show();

			yes.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					/*							lazyAdapter.clear();*/
					if(listObject.size()!=0){
						objSerial.serialize((Object)listObject,MDHEALTH);
					}
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					if(isFinish){
						dialog.dismiss();  
						LazyAdapter.imageloader.clearCache();
						if(lazyAdapter!=null)
						{
							LazyAdapter.clear();
						}
						Intent intent = new Intent(context,MadhuriHomeScreen.class);
						startActivity(intent);
						//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
						downloadFile.cancel(true);
						finish();
					}else{
						dialog.dismiss(); 
						LazyAdapter.imageloader.clearCache();
						if(lazyAdapter!=null)
						{
							LazyAdapter.clear();
						}
						downloadFile.cancel(true);
						finish();
						//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);


						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
					}
				}
			});

			no.setOnClickListener(new OnClickListener()
			{


				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();  
				}
			});

			/*dialog.show();*/
		}
		else{
			if(listObject.size()!=0){
				objSerial.serialize((Object)listObject,MDHEALTH);
			}
			/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
			LazyAdapter.imageloader.clearCache();
			if(lazyAdapter!=null)
			{
				LazyAdapter.clear();
			}
			super.onBackPressed();

			if(fade)
			{
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
			}
			else
			{
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		/*
		 * Persisting State of List
		 */
		try{
			if(listObject.size()!=0){
				objSerial.serialize((Object)listObject,MDHEALTH);
			}
		}catch(NullPointerException np){
			np.printStackTrace();
		}catch(Exception xp){
			xp.printStackTrace();
		}
		super.onPause();
	}

}
