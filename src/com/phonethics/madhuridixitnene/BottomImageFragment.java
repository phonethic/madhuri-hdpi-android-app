package com.phonethics.madhuridixitnene;



import java.io.File;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;



public class BottomImageFragment extends Fragment{
	Context context;
	String imgname;
	String btnName;
	int positionTag;
	MediaPlayer tapSound;
	
	com.nostra13.universalimageloader.core.ImageLoader imageLoader;
	File cacheDir;
	DisplayImageOptions options;
	ImageLoaderConfiguration config;
	
	public BottomImageFragment()
	{

	}
	public BottomImageFragment(Context context,String imgname,String btnName,int position)
	{
		this.context=context;
		this.imgname=imgname;
		this.btnName=btnName;
		positionTag=position;
		tapSound=MediaPlayer.create(context, R.raw.i_button_sound);

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		
		imageLoader =ImageLoader.getInstance();
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MDMAIN");
		else
			cacheDir=context.getCacheDir();
		if(!cacheDir.exists())
			cacheDir.mkdirs();
		
		/*config = new ImageLoaderConfiguration.Builder(context)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.discCache(new UnlimitedDiscCache(cacheDir))
		.denyCacheImageMultipleSizesInMemory()
	
		.imageDownloader(new ExtendedImageDownloader(context))
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.enableLogging() // Not necessary in common
		.build();
		imageLoader.init(config);*/
		
		
		/*config = new ImageLoaderConfiguration.Builder(context)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.denyCacheImageMultipleSizesInMemory()
		.memoryCache(new WeakMemoryCache())
		.memoryCacheSize(2*1024*1024)
		.imageDownloader(new ExtendedImageDownloader(context))
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.enableLogging() // Not necessary in common
		.build();
		imageLoader.init(config);
		

		options = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.build();
		*/

		
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(savedInstanceState!=null)
		{
			imgname=savedInstanceState.getString("imgName");
			btnName=savedInstanceState.getString("btnName");
			context=getActivity().getApplicationContext();
		}
		
		
		View v=(View)inflater.inflate(R.layout.pagerimage,null);
		try
		{
		final ImageView imgv=(ImageView)v.findViewById(R.id.imageView1);

		final ImageView imgToogle=(ImageView)v.findViewById(R.id.togButton);

		
		imgv.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())));
		imgv.setTag(positionTag);

		imgToogle.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName, "drawable",context.getPackageName())));

		
		
		
		/*imageLoader.displayImage("drawable://"+context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName()), imgv, options);*/
		
		
		
		if(positionTag%2==0 && positionTag!=0)
		{
			imgToogle.setClickable(true);

			final StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {android.R.attr.state_pressed},
					getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName+"_selected", "drawable",context.getPackageName())));
			states.addState(new int[] {android.R.attr.state_focused},
					getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName+"_selected", "drawable",context.getPackageName())));
			states.addState(new int[] { },
					getResources().getDrawable(context.getResources().getIdentifier("drawable/"+btnName, "drawable",context.getPackageName())));
			
			
		


			imgToogle.setOnTouchListener(new OnTouchListener() {
				int count=0;
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					imgToogle.setImageDrawable(states);
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						imgv.setImageResource(R.drawable.slider_glow);
						break;
					case MotionEvent.ACTION_UP:
						imgv.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())));
						break;
					default:
						imgv.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())));
						break;
					}
					
					return false;
				}
			});

			imgToogle.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					switch(positionTag)
					{
					case 2: //latest
					{
						
					//	imgv.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())));
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),LatestTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						/*imgv.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgname, "drawable",context.getPackageName())));*/
						break;
					}
					case 4: //dance
					{
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),DanceTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						break;
					}
					case 6: //movies
					{
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),MoviesTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						break;
					}
					case 8://fashion
					{
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),FashionTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						break;
					} 
					case 10: //health
					{
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),HealthTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						break;
					}
					case 12: //art
					{
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),ArtTab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						break;
					}
					case 14: //gallery
					{
						tapSound.start();
						Intent intent=new Intent(getActivity().getApplicationContext(),Gallery_Tab.class);
						intent.putExtra("finishAct", false);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						break;
					}

					}
				}
			});
		}
		else
		{
			imgToogle.setClickable(false);
		

		}

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return v;
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("imgName", imgname);
		outState.putString("btnName", btnName);
		context=getActivity().getApplicationContext();

	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if(savedInstanceState!=null)
		{
			imgname=savedInstanceState.getString("imgName");
			btnName=savedInstanceState.getString("btnName");
			context=getActivity().getApplicationContext();
		}
	}

	
	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize =1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float)height / (float)reqHeight);
			} else {
				inSampleSize = Math.round((float)width / (float)reqWidth);
			}
		}
		Log.i("", "Sample Size"+inSampleSize);
		return inSampleSize;
	}



	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inPurgeable=true;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inScaled = false;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}





}
