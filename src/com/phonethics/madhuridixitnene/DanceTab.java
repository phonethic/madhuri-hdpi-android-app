package com.phonethics.madhuridixitnene;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.PushService;





public class DanceTab extends Activity {

	Activity 							context;
	ArrayList<String> 					contentList,slugList, dataDance, imgUrl;
	ArrayList<HashMap<String, String>> 	mylist;
	JSONObject 							json, obj,img,thumb,objJson;
	JSONArray  							dance,attachment;
	ImageView 							bck;
	String 								shareUrl ="http://madhuridixit-nene.com/dance/post/";
	String 								danceUrl = "http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=dance&count=-1";
	String 								jString,imgStr;
	ImageView 							showProgress;
	NetworkCheck 						netCheck;
	//File 								externalFile 	= new File(Environment.getExternalStorageDirectory(),"MadhuriDance.bin");
	PullToRefreshListView 				listView;
	MyAsyncTask							downloadFile;
	ImageLoader 						imageLoader;
	LazyAdapter 						lazyadapter;
	Bundle 								bundle;
	boolean 							isFinish = true, errorCaught = false, isRefreshing = false;
	int 								p=0;
	Dialog 								dialog;
	int									presentSize=0, newSize,dataSize=0;
	boolean									fade;
	int										CACHE_VALIDITY;


	JSONObject								configJson,tempJson;
	JSONArray								categoryJson;
	ImageView 								btnLoadMore;
	ObjectSerializer						objSerial;
	ListViewContent							listContetnt;
	PostCount								postCount;
	TabConfig								tabAsync;
	String 									danceUrlWithCount = "http://madhuridixit-nene.com/wp/?json=get_category_posts&dev=1&slug=dance&page=";
	String									TAB_INFO ="/mnt/sdcard/MdNTabpostConfig.bin";
	String									TABINFO_URL="http://madhuridixit-nene.com/wp/?json=get_category_index&dev=1";
	File 									serialFile = new File(Environment.getExternalStorageDirectory(),"MdNDance.bin");
	File									configFile= new File(Environment.getExternalStorageDirectory(),"MdNTabpostConfig.bin");
	int										pageCount=1,totalPages,TOTAL_POST_COUNT;
	ArrayList<Integer> 						postID;
	ArrayList<ListViewContent> 				listObject;
	ArrayList<PostCount> 					postConfigArrray;
	String									MDDANCE = "/mnt/sdcard/MdNDance.bin";
	String									THIS_TAB = "Dance";



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dance_tab);

		context							=	this;
		/*	PushService.setDefaultPushCallback(context, DanceTab.class);*/
		bck 							= (ImageView) findViewById(R.id.bckbtn);
		showProgress					= (ImageView)findViewById(R.id.showProgress);
		netCheck 						= new NetworkCheck(context);
		listView 						= (PullToRefreshListView) findViewById(R.id.dancelist);
		mylist 							= new ArrayList<HashMap<String, String>>();
		contentList						= new ArrayList<String>();
		slugList						= new ArrayList<String>();
		dataDance						= new ArrayList<String>();
		imgUrl							= new ArrayList<String>();
		lazyadapter						= new LazyAdapter(context);
		dialog 							= new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertbox);
		listView.setVisibility(View.GONE);



		objSerial	=		new ObjectSerializer();
		btnLoadMore = 		(ImageView) findViewById(R.id.btnLoadMore);
		postID		=		new ArrayList<Integer>();
		listObject			= new ArrayList<ListViewContent>();
		postConfigArrray	= new  ArrayList<PostCount>();
		tabAsync			= new TabConfig();

		downloadFile = new MyAsyncTask();

		CACHE_VALIDITY = Integer.parseInt(getResources().getString(R.string.CacheFileValidity));

		bundle = getIntent().getExtras();
		if(bundle != null){
			isFinish = bundle.getBoolean("finishAct");

			fade=bundle.getBoolean("shtcut");
		}


		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.list_layout_controller);
		listView.setLayoutAnimation(controller);

		if(netCheck.isNetworkAvailable()){
			/*if(externalFile.exists()){
				Date lastModDate = new Date(externalFile.lastModified());
				Date todayDate = new Date();
				long fileDate = lastModDate.getTime();
				long currentDate = todayDate.getTime();
				long dateDiff = currentDate - fileDate;
				long diffDays = dateDiff / (24 * 60 * 60 * 1000);

				if(diffDays > CACHE_VALIDITY){
					Log.d("************", "-------NO OF DAYS DIFFERENCE-------" +diffDays);
					showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
					showProgress.setVisibility(View.VISIBLE);
					downloadFile.execute(danceUrl);
				}else{
					try {
						FileInputStream filename 				= null;
						Document xmldoc	 						= null;
						DocumentBuilderFactory doc_build_fact 	= null;
						DocumentBuilder doc_builder 			= null;
						filename = new FileInputStream("/sdcard/MadhuriDance.txt");



						FileChannel fc = filename.getChannel();
						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						jString = Charset.defaultCharset().decode(bb).toString();
						json = new JSONObject(jString); 
						dance = json.getJSONArray("posts");
						imgUrl.clear();
						dataDance.clear();
						objJson = json.getJSONObject("category");
						dataSize = objJson.getInt("post_count");
						for(int i=0;i<dance.length();i++){

							HashMap<String, String> map = new HashMap<String, String>();	
							JSONObject e = dance.getJSONObject(i);

							String name = e.getString("title_plain");
							String content = e.getString("content");
							if(e.has("thumbnail")){
								imgStr	= e.getString("thumbnail");
							}else{
								Log.d("-----", "NO thumbnail at position ---- " + i);

							attachment = e.getJSONArray("attachments");
							for(int p=0; p<attachment.length();p++){
								obj = attachment.getJSONObject(p);
								img = obj.getJSONObject("images");
								thumb = img.getJSONObject("thumbnail");
								imgStr = thumb.getString("url");
							}
							}
							contentList.add(content);

							String chng = "&#8230;";
							String 	Sub_fnl_Str = name.replace(chng, " ");
							String chng1 = "&#8211;";
							String fnl_Str = Sub_fnl_Str.replace(chng1, "");




							String slug = e.getString("slug");
							slugList.add(slug);
							dataDance.add(fnl_Str);
							imgUrl.add(imgStr);
							map.put("id",  String.valueOf(i));
							map.put("name", fnl_Str);
							map.put("date", e.getString("date"));

							mylist.add(map);			
						}		
					}catch(JSONException e)        {
						Log.e("log_tag", "Error parsing data "+e.toString());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch (Exception e){
						Log.e("Error: ", " "+e.getMessage());
					}

					showProgress.clearAnimation();
					showProgress.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
					adapter = new CustomListAdapter(context, dataDance, imgUrl);
					listView.setAdapter(adapter);

					lazyadapter = new LazyAdapter(context, imgUrl, dataDance);
					listView.setAdapter(lazyadapter);
					lazyadapter.notifyDataSetChanged();
				}
			}else{
				showProgress.setVisibility(View.VISIBLE);
				showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				downloadFile.execute(danceUrl);
			}*/


			/*
			 *  Getting total post count and serailizing it
			 */

			if(!configFile.exists() || configFile.length()==0){
				/*
				 *  If configFile Dosent exist or file length is zero then serialize it.
				 */
				tabAsync.execute(TABINFO_URL);
			}else{
				/*
				 * Deserializing  and store  post count
				 */
				Object deserialTabInfo =  objSerial.deserialize(TAB_INFO);
				postConfigArrray = (ArrayList<PostCount>)deserialTabInfo;
				for(int i=0;i<postConfigArrray.size();i++){
					if(postConfigArrray.get(i).getTabName().equalsIgnoreCase(THIS_TAB)){
						TOTAL_POST_COUNT = postConfigArrray.get(i).getPostCount();

					}
				}

				Log.i("*******", "Total POst " + TOTAL_POST_COUNT);
			}

			if(serialFile.exists()){

				/*
				 *  if network is available and file exist deserialize it 
				 */

				Object	deserial = objSerial.deserialize(MDDANCE);
				listObject=(ArrayList<ListViewContent>)deserial;
				for(int r=0; r<listObject.size();r++){
					imgUrl.add(listObject.get(r).getThumbnailLink());
					dataDance.add(listObject.get(r).getTitle());
					slugList.add(listObject.get(r).getSlug());
					contentList.add(listObject.get(r).getContentWebLink());
					pageCount = listObject.get(r).getPageState();
					postID.add(listObject.get(r).getPostId());
				}

				lazyadapter = new LazyAdapter(context, imgUrl, dataDance);
				listView.setAdapter(lazyadapter);
				listView.setVisibility(View.VISIBLE);
				lazyadapter.notifyDataSetChanged();
				if(dataDance.size()< TOTAL_POST_COUNT){
					btnLoadMore.setVisibility(View.VISIBLE);
				}
				Log.i("******0---*", "Dance Data " + dataDance.size());
			}else{
				/*
				 *  net connected but no such file exist then download content
				 */


				Log.d("************", "-------EXTERNAL FILOE WAS NOT THERE DOWNLOADING NEW ONE-------");
				showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
				showProgress.setVisibility(View.VISIBLE);
				downloadFile.execute(danceUrlWithCount+pageCount);
			}



		}else{
			//Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			/*if(externalFile.exists()){
				try {
					FileInputStream filename 				= null;
					Document xmldoc	 						= null;
					DocumentBuilderFactory doc_build_fact 	= null;
					DocumentBuilder doc_builder 			= null;
					filename = new FileInputStream("/sdcard/MadhuriDance.txt");



					FileChannel fc = filename.getChannel();
					MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
					jString = Charset.defaultCharset().decode(bb).toString();
					json = new JSONObject(jString); 
					dance = json.getJSONArray("posts");
					imgUrl.clear();
					dataDance.clear();
					objJson = json.getJSONObject("category");
					dataSize = objJson.getInt("post_count");
					for(int i=0;i<dance.length();i++){

						HashMap<String, String> map = new HashMap<String, String>();	
						JSONObject e = dance.getJSONObject(i);

						String name = e.getString("title_plain");
						String content = e.getString("content");
						if(e.has("thumbnail")){
							imgStr	= e.getString("thumbnail");
						}else{
							Log.d("-----", "NO thumbnail at position ---- " + i);

						attachment = e.getJSONArray("attachments");
						for(int p=0; p<attachment.length();p++){
							obj = attachment.getJSONObject(p);
							img = obj.getJSONObject("images");
							thumb = img.getJSONObject("thumbnail");
							imgStr = thumb.getString("url");
						}
						}
						contentList.add(content);

						String chng = "&#8230;";
						String 	Sub_fnl_Str = name.replace(chng, " ");
						String chng1 = "&#8211;";
						String fnl_Str = Sub_fnl_Str.replace(chng1, "");




						String slug = e.getString("slug");
						slugList.add(slug);
						dataDance.add(fnl_Str);
						imgUrl.add(imgStr);
						map.put("id",  String.valueOf(i));
						map.put("name", fnl_Str);
						map.put("date", e.getString("date"));

						mylist.add(map);			
					}		
				}catch(JSONException e)        {
					Log.e("log_tag", "Error parsing data "+e.toString());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch (Exception e){
					Log.e("Error: ", " "+e.getMessage());
				}

				showProgress.clearAnimation();
				showProgress.setVisibility(View.GONE);
				listView.setVisibility(View.VISIBLE);
				adapter = new CustomListAdapter(context, dataDance, imgUrl);
				listView.setAdapter(adapter);

				lazyadapter = new LazyAdapter(context, imgUrl, dataDance);
				listView.setAdapter(lazyadapter);
				lazyadapter.notifyDataSetChanged();


				listView.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		


						Intent intent=new Intent(context,WebViewActivity.class);
						intent.putExtra("content", contentList.get(position));
						intent.putExtra("shareUrl", shareUrl+slugList.get(position));
						intent.putExtra("tab_value", 2);
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

					}
				});
			}catch(JSONException e)        {
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}else{
				Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			}
			 */




			/*
			 * 
			 *  If net is not connected and file exist
			 */
			if(serialFile.exists()){
				Object	deserial = objSerial.deserialize(MDDANCE);
				listObject=(ArrayList<ListViewContent>)deserial;
				for(int r=0; r<listObject.size();r++){
					imgUrl.add(listObject.get(r).getThumbnailLink());
					dataDance.add(listObject.get(r).getTitle());
					slugList.add(listObject.get(r).getSlug());
					contentList.add(listObject.get(r).getContentWebLink());
					pageCount = listObject.get(r).getPageState();
					postID.add(listObject.get(r).getPostId());
				}
				lazyadapter = new LazyAdapter(context, imgUrl, dataDance);
				listView.setAdapter(lazyadapter);
				listView.setVisibility(View.VISIBLE);
				lazyadapter.notifyDataSetChanged();

			}else{

				Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
			}
		}

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		

				if(!isRefreshing){
					Intent intent=new Intent(context,WebViewActivity.class);
					intent.putExtra("content", contentList.get(position-1));
					intent.putExtra("shareUrl", shareUrl+slugList.get(position-1));
					intent.putExtra("tab_value", 2);
					startActivity(intent);
					overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
			}
		});


		/*
		 * Loading More records 
		 */
		btnLoadMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(   (!isRefreshing) && (!showProgress.isShown())  ){
					if(pageCount < totalPages || totalPages == 0){
						pageCount += 1;

						showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
						showProgress.setVisibility(View.VISIBLE);
						new MyAsyncTask().execute(danceUrlWithCount+pageCount);
					}
				}
			}
		});


		bck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(showProgress.isShown() || isRefreshing){

					/*//final Dialog dialog = new Dialog(Tab.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
					dialog.setContentView(R.layout.alertbox);*/

					ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
					ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);

					yes.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v) 
						{
							/*			lazyadapter.clear();*/

							if(listObject.size()!=0){
								objSerial.serialize((Object)listObject,MDDANCE);
							}
							/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
							if(isFinish){
								dialog.dismiss(); 
								LazyAdapter.imageloader.clearCache();
								if(lazyadapter!=null)
								{
									LazyAdapter.clear();
								}
								downloadFile.cancel(true);

								Intent intent = new Intent(context,MadhuriHomeScreen.class);
								startActivity(intent);

								//	overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}

								finish();

							}else{
								dialog.dismiss(); 
								LazyAdapter.imageloader.clearCache();
								if(lazyadapter!=null)
								{
									LazyAdapter.clear();
								}
								downloadFile.cancel(true);
								finish();
								//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								if(fade)
								{
									overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
								}
								else
								{
									overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
								}

							}
						}
					});

					no.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v) 
						{
							dialog.dismiss();  

						}
					});

					dialog.show();

				}else{
					if(listObject.size()!=0){
						objSerial.serialize((Object)listObject,MDDANCE);
					}
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
					LazyAdapter.imageloader.clearCache();
					if(lazyadapter!=null)
					{
						LazyAdapter.clear();
					}
					downloadFile.cancel(true);
					finish();
					//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					if(fade)
					{
						overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					}
					else
					{
						overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
					}
				}
			}
		});


		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				/*	refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);*/

				// Do work to refresh the list here.
				if(netCheck.isNetworkAvailable()){
					isRefreshing = true;
					/*try {
						FileInputStream filename 				= null;
						Document xmldoc	 						= null;
						DocumentBuilderFactory doc_build_fact 	= null;
						DocumentBuilder doc_builder 			= null;
						filename = new FileInputStream("/sdcard/MadhuriDance.txt");

						FileChannel fc = filename.getChannel();
						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						jString = Charset.defaultCharset().decode(bb).toString();
						json = new JSONObject(jString); 
						objJson = json.getJSONObject("category");
						presentSize = objJson.getInt("post_count");

					}catch(JSONException e){
						Log.e("log_tag", "Error parsing data "+e.toString());

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					catch (Exception e){
						Log.e("Error: ", " "+e.getMessage());
					}
					Url = "http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=fashion&count=-1";



					new MyAsyncTask().execute(danceUrl);*/




					if(!showProgress.isShown()){
						new TabConfig().execute(TABINFO_URL);
					}else{
						listView.onRefreshComplete();
					}

				}else{
					listView.onRefreshComplete();
				}
			}
		});


	}

	class MyAsyncTask extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>> > {
		boolean exceptionOccured  = false;

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {
			// TODO Auto-generated method stub

			InputStream is = null;
			String result = "";
			JSONObject jObj = null;

			try{

				try{
					
					HttpGet httpGet = new HttpGet(params[0]);
					HttpParams httpParams = new BasicHttpParams();

					int timeoutConnection = 30000;
					HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 30000;
					HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

					DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
					HttpResponse response = httpClient.execute(httpGet);

					HttpEntity entity = response.getEntity();
					is = entity.getContent();

				}catch(Exception ex){
					Log.e("log_tag", "Error in http connection "+ex.toString());
					ex.printStackTrace();
					exceptionOccured = true;
				}

				try{
					if(!exceptionOccured){
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						StringBuilder	sb  = new StringBuilder();
						String line = null;

						while ((line = reader.readLine())!=null) {
							sb.append(line + "\n");

						}
						is.close();
						result = sb.toString();
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error converting result "+ex.toString());
					exceptionOccured = true;
				}

				//try parse the string to a JSON object
				try{
					if(!exceptionOccured){
						jObj = new JSONObject(result);
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error parsing data "+ex.toString());
					exceptionOccured = true;
				}


				if(!exceptionOccured){


					//json = JSONfunctions.getJSONfromURL(params[0]);
					objJson = jObj.getJSONObject("category");
					dataSize = objJson.getInt("post_count");
					totalPages = jObj.getInt("pages");

					dance = jObj.getJSONArray("posts");
					if(isRefreshing && pageCount == 1){
						contentList.clear();
						slugList.clear();
						dataDance.clear();
						imgUrl.clear();
						postID.clear();
						listObject.clear();
					}
					for(int i=0;i<dance.length();i++){	

						HashMap<String, String> map = new HashMap<String, String>();	
						JSONObject e = dance.getJSONObject(i);

						String name = e.getString("title_plain");
						String content = e.getString("content");
						int postId = e.getInt("id");

						attachment = e.getJSONArray("attachments");

						for(int p=0; p<attachment.length();p++){
							/*	obj = attachment.getJSONObject(p);
						img = obj.getJSONObject("images");
						thumb = img.getJSONObject("thumbnail");
						imgStr = thumb.getString("url");*/


							obj = attachment.getJSONObject(p);
							img = null;
							try{
								img = obj.getJSONObject("images");
							}catch(Exception ex){
								ex.printStackTrace();
							}
							if(img!=null){
								thumb= null;
								try{
									thumb = img.getJSONObject("thumbnail");
								}catch(Exception ex){
									ex.printStackTrace();
								}
								if(thumb!=null){
									imgStr = thumb.getString("url");	
								}else{
									imgStr="";
								}	
							}else{
								imgStr="";	
							}
						}	
						String 	fnl_Str="";
						String slug = "";

						if((postID.size()== 0) || (!postID.contains(postId))){
							contentList.add(content);
							String chng = "&#8230;";
							String 	Sub_fnl_Str = name.replace(chng, " ");
							String chng1 = "&#8211;";
							fnl_Str = Sub_fnl_Str.replace(chng1, "");
							slug = e.getString("slug");
							slugList.add(slug);
							dataDance.add(fnl_Str);
							imgUrl.add(imgStr);
							postID.add(postId);


							listContetnt		= new ListViewContent();
							listContetnt.setContentWebLink(content);
							listContetnt.setPostId(postId);
							listContetnt.setThumbnailLink(imgStr);
							listContetnt.setTitle(fnl_Str);
							listContetnt.setSlug(slug);
							listContetnt.setShareUrl(shareUrl);
							listContetnt.setPageState(pageCount);
							listObject.add(listContetnt);
						}

						map.put("id",  String.valueOf(i));
						map.put("name", fnl_Str);
						map.put("date", e.getString("date"));


						mylist.add(map);			
					}
				}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return mylist;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			if(exceptionOccured){
				Toast.makeText(context, "Network Error", 0).show();
				listView.onRefreshComplete();
			}else{
				if(dataDance.size() < TOTAL_POST_COUNT){
					btnLoadMore.setVisibility(View.VISIBLE);
				}else{
					btnLoadMore.setVisibility(View.GONE);
				}
				showProgress.clearAnimation();
				btnLoadMore.setClickable(true);
				showProgress.setVisibility(View.GONE);
				listView.setVisibility(View.VISIBLE);
				isRefreshing = false;
				if(dialog.isShowing()){
					dialog.dismiss();
				}

				if(errorCaught){
					Toast.makeText(context, "Internet Connection Lost", Toast.LENGTH_SHORT).show();
				}else{
					lazyadapter=null;
					lazyadapter = new LazyAdapter(context, imgUrl, dataDance);
					listView.setAdapter(lazyadapter);
					lazyadapter.notifyDataSetChanged();
				}


				if(pageCount == totalPages){
					btnLoadMore.setVisibility(View.GONE);
				}else{
					btnLoadMore.setVisibility(View.VISIBLE);
				}


				listView.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		

						if(!isRefreshing){
							Intent intent=new Intent(context,WebViewActivity.class);
							intent.putExtra("content", contentList.get(position-1));
							intent.putExtra("shareUrl", shareUrl+slugList.get(position-1));
							intent.putExtra("tab_value", 2);
							startActivity(intent);
							overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						}
					}
				});
				listView.onRefreshComplete();
			}

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			listView.setOnItemClickListener(null);
			btnLoadMore.setClickable(false);
			super.onPreExecute();

		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub

			super.onProgressUpdate(values);

		}






	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_dance_tab, menu);
		return false;
	}

	class TabConfig extends AsyncTask<String, Integer, ArrayList<HashMap<String, Integer>> >{
		boolean exceptionOccured  = false;

		@Override
		protected ArrayList<HashMap<String, Integer>> doInBackground(
				String... params) {

			// TODO Auto-generated method stub



			int count;
			InputStream is = null;
			String result = "";
			JSONObject jObj = null;


			try {

				try{
					/*DefaultHttpClient  httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(params[0]);*/

					HttpGet httpGet = new HttpGet(params[0]);
					HttpParams httpParams = new BasicHttpParams();

					int timeoutConnection = 30000;
					HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
					// Set the default socket timeout (SO_TIMEOUT) 
					// in milliseconds which is the timeout for waiting for data.
					int timeoutSocket = 30000;
					HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

					DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
					HttpResponse response = httpClient.execute(httpGet);

					HttpEntity entity = response.getEntity();
					is = entity.getContent();

				}catch(Exception ex){
					Log.e("log_tag", "Error in http connection "+ex.toString());
					ex.printStackTrace();
					exceptionOccured = true;
				}



				//convert response to string
				try{
					if(!exceptionOccured){
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						StringBuilder	sb  = new StringBuilder();
						String line = null;

						while ((line = reader.readLine())!=null) {
							sb.append(line + "\n");

						}
						is.close();
						result = sb.toString();
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error converting result "+ex.toString());
					exceptionOccured = true;
				}

				//try parse the string to a JSON object
				try{
					if(!exceptionOccured){
						jObj = new JSONObject(result);
					}
				}catch(Exception ex){
					Log.e("log_tag", "Error parsing data "+ex.toString());
					exceptionOccured = true;
				}


				if(!exceptionOccured){
					categoryJson	=	jObj.getJSONArray("categories");

					for(int i=0; i <categoryJson.length(); i++){
						tempJson = categoryJson.getJSONObject(i);
						String title = tempJson.getString("title");

						int post_count = tempJson.getInt("post_count");
						if(title.equalsIgnoreCase(THIS_TAB)){
							TOTAL_POST_COUNT = post_count;
						}
						Log.i("*** Asynch **", "Total Post " + TOTAL_POST_COUNT);
						postCount = new PostCount();
						postCount.setPostCount(post_count);
						postCount.setTabName(title);
						postConfigArrray.add(postCount);
					}
				}
			} catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e){
				Log.e("Error: ", " "+e.getMessage());
			}


			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, Integer>> result) {
			// TODO Auto-generated method stub
			if(exceptionOccured){
				Toast.makeText(context, "Network Error", 0).show();
				listView.onRefreshComplete();
			}else{
				objSerial.serialize((Object)postConfigArrray, TAB_INFO);
				for(int i=0;i<postConfigArrray.size();i++){
					if(postConfigArrray.get(i).getTabName().equalsIgnoreCase(THIS_TAB)){
						TOTAL_POST_COUNT = postConfigArrray.get(i).getPostCount();

					}
				}
				/*if(dataDance.size()< TOTAL_POST_COUNT){
					btnLoadMore.setVisibility(View.VISIBLE);
					if(isRefreshing){
						new MyAsyncTask().execute(danceUrlWithCount+pageCount);
					}
				}*/
				if(isRefreshing){
					pageCount = 1;
					new MyAsyncTask().execute(danceUrlWithCount+pageCount);
				}
				else{
					btnLoadMore.setVisibility(View.GONE);
					listView.onRefreshComplete();
					isRefreshing = false;
					if(dialog.isShowing()){
						dialog.dismiss();
					}
				}
			}
			super.onPostExecute(result);
		}
	}




	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*lazyadapter.clear();*/
		/*if(isFinish){
			Intent intent = new Intent(context,MadhuriHomeScreen.class);
			startActivity(intent);
			finish();

			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}else{
			super.onBackPressed();

			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		}
		 */

		if(showProgress.isShown() || isRefreshing){

			/*//final Dialog dialog = new Dialog(Tab.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawableResource(R.drawable.alert_box);
			dialog.setContentView(R.layout.alertbox);*/

			ImageView yes = (ImageView) dialog.findViewById(R.id.yesbtn);
			ImageView no = (ImageView) dialog.findViewById(R.id.nobtn);
			dialog.show();

			yes.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					/*							lazyAdapter.clear();*/

					if(listObject.size()!=0){
						objSerial.serialize((Object)listObject,MDDANCE);
					}
					/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/

					if(isFinish){
						dialog.dismiss();  
						LazyAdapter.imageloader.clearCache();
						if(lazyadapter!=null)
						{
							LazyAdapter.clear();
						}
						downloadFile.cancel(true);

						Intent intent = new Intent(context,MadhuriHomeScreen.class);
						startActivity(intent);
						//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}

						finish();
					}else{
						dialog.dismiss();  
						LazyAdapter.imageloader.clearCache();
						if(lazyadapter!=null)
						{
							LazyAdapter.clear();
						}
						downloadFile.cancel(true);
						finish();
						//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						if(fade)
						{
							overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
						}
						else
						{
							overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
						}
					}
				}
			});

			no.setOnClickListener(new OnClickListener()
			{


				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();  
				}
			});

			/*dialog.show();*/
		}
		else{

			if(listObject.size()!=0){
				objSerial.serialize((Object)listObject,MDDANCE);
			}
			/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
			LazyAdapter.imageloader.clearCache();
			if(lazyadapter!=null)
			{
				LazyAdapter.clear();
			}
			super.onBackPressed();
			//overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			if(fade)
			{
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
			}
			else
			{
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		}

	}


	@Override
	public void finish() {
		// TODO Auto-generated method stub
		try{
			LazyAdapter.imageloader.clearCache();
			Log.i("^^^^^^ In Finish  ", "clear chache ");
		}catch(NullPointerException npEx){
			npEx.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.finish();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		/*
		 * Persisting State of List
		 */
		try{
			if(listObject.size()!=0){
				objSerial.serialize((Object)listObject,MDDANCE);
			}
		}catch(NullPointerException np){
			np.printStackTrace();
		}catch(Exception xp){
			xp.printStackTrace();
		}
		super.onPause();
	}

}
