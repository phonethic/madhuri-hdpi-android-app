package com.phonethics.madhuridixitnene;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Handler;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class FixedSpeedScroller extends Scroller {

    private int mDuration = 2500;
    

    public FixedSpeedScroller(Context context) {
        super(context);
    }

    public FixedSpeedScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
        
    }

    @TargetApi(11)
	public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
        super(context, interpolator, flywheel);
    }

    
    @Override
	public void fling(int startX, int startY, int velocityX, int velocityY,
			int minX, int maxX, int minY, int maxY) {
		// TODO Auto-generated method stub
		super.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
	}

	@Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        // Ignore received duration, use fixed one instead
        super.startScroll(startX, startY, dx, dy, mDuration);
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy) {
        // Ignore received duration, use fixed one instead
        super.startScroll(startX, startY, dx, dy, mDuration);
    }
    
    public void setFixedDuration(int mDuration)
    {
    	this.mDuration=mDuration; 
    }
}