package com.phonethics.madhuridixitnene;




import com.flurry.android.FlurryAgent;
import com.parse.PushService;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ShortcutMenus extends Activity {

	RelativeLayout closelay;
	ImageView closebtn,latest,dance,movies,fashion,health,art,gallery,url;
	Context context;
	Bundle bundle;
	boolean finisshAct = true;
	MediaPlayer tapSound;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_shortcut_menus);

		tapSound=MediaPlayer.create(getApplicationContext(), R.raw.i_button_sound);
		closelay = (RelativeLayout) findViewById(R.id.closebtnlayout);

		closebtn = (ImageView) findViewById(R.id.closebtn);
		latest = (ImageView) findViewById(R.id.menu1);
		dance = (ImageView) findViewById(R.id.menu2);
		movies = (ImageView) findViewById(R.id.menu3);
		fashion = (ImageView) findViewById(R.id.menu4);
		health = (ImageView) findViewById(R.id.menu5);
		art = (ImageView) findViewById(R.id.menu6);
		gallery = (ImageView) findViewById(R.id.menu7);
		url = (ImageView) findViewById(R.id.menu8);

		context = this;
		/*PushService.setDefaultPushCallback(context, ShortcutMenus.class);*/
		bundle = getIntent().getExtras();
		if(bundle != null){
			finisshAct = bundle.getBoolean("finishAct");
		}

		closelay.bringToFront();
		closebtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*Intent intent=new Intent(context,Thread_Test.class);
				startActivity(intent);*/
				/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
				finish();
				overridePendingTransition(0,R.anim.activityfade_out);

			}
		});

		latest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,LatestTab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					//overridePendingTransition(enterAnim, exitAnim)
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}
			}
		});

		dance.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,DanceTab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}
			}
		});

		movies.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,MoviesTab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}

			}
		});

		fashion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,FashionTab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}
			}
		});

		health.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,HealthTab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}
			}
		});

		art.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,ArtTab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}
			}
		});

		gallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tapSound.start();
				Intent intent=new Intent(context,Gallery_Tab.class);
				if(!finisshAct){
					intent.putExtra("finishAct", false);
					intent.putExtra("shtcut",true);
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}else{
					startActivity(intent);
					overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);
					finish();
				}
			}
		});

		url.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			/*	Uri uri = Uri.parse("http://phonethics.in/");
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);*/
				try
				{
					FlurryAgent.logEvent("Phonethics_Website");
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				Intent intent=new Intent(context,FbTwtrLink.class);
				intent.putExtra("sitelink", 3);
				startActivity(intent);
				overridePendingTransition(R.anim.grow_fade_in_center,R.anim.shrink_fade_out_center);

			}
		});
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "456H2RVTM3XG7H86MK95");
		
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		FlurryAgent.onEndSession(this);
		
	}
	
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*PushService.setDefaultPushCallback(context, MadhuriHomeScreen.class);*/
		super.onBackPressed();
		overridePendingTransition(0,R.anim.activityfade_out);
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_shortcut_menus, menu);
		return false;
	}
}
