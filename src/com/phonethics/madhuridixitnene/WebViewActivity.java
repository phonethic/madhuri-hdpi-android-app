package com.phonethics.madhuridixitnene;




import com.parse.PushService;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.widget.ImageView;

public class WebViewActivity extends Activity {

	WebView web;
	ImageView img,bckimg,share_btn;
	String content,shareUrl;
	String movYear, movName, movImage, movDirector, movAwards; 
	String movProducer, movActedAs, movAnecdotes, movSynopsis; 
	boolean thisUrl = false;
	ImageView showProgress;
	Context context;
	int tab_value;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_web_view);
		context = this;
		/*PushService.setDefaultPushCallback(context, WebViewActivity.class);*/
		Bundle bundle=getIntent().getExtras();
		showProgress=(ImageView)findViewById(R.id.showProgress);

		img = (ImageView)findViewById(R.id.header);
		bckimg = (ImageView) findViewById(R.id.wbview_bckbtn);
		share_btn = (ImageView) findViewById(R.id.share_btn);

		web=(WebView)findViewById(R.id.webview);
		/*web.getSettings().setJavaScriptEnabled(true);
		web.getSettings().setPluginsEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		web.getSettings().setAllowFileAccess(true);
		web.getSettings().setBuiltInZoomControls(true);
		web.getSettings().setBlockNetworkLoads(false);*/
		
		web.getSettings().setJavaScriptEnabled(true);
		
		web.getSettings().setPluginsEnabled(true);
		web.getSettings().setPluginState(PluginState.ON);
		web.getSettings().setAllowFileAccess(true);
		web.getSettings().setBuiltInZoomControls(true);
		
		web.setWebChromeClient(new WebChromeClient() {

			@Override
			public void onHideCustomView() {
				// TODO Auto-generated method stub
				super.onHideCustomView();
			}

			@Override
			public void onShowCustomView(View view, CustomViewCallback callback) {
				// TODO Auto-generated method stub
				if(!view.isShown())
				{
					view.setVisibility(View.VISIBLE);
				}
				super.onShowCustomView(view, callback);
			}

			

			
			
		});


		web.setWebViewClient(new MyWebViewClient());

		if(bundle!=null)
		{
			tab_value = bundle.getInt("tab_value");

			if(tab_value==1){
				img.setImageResource(R.drawable.bar_latest);

				thisUrl = false;
			}else if(tab_value==2){
				img.setImageResource(R.drawable.bar_dance);
				thisUrl = false;
			}else if(tab_value==3){
				img.setImageResource(R.drawable.bar_movies);
				thisUrl = true;
			}else if(tab_value==4){
				img.setImageResource(R.drawable.bar_fashion);
				thisUrl = false;
			}else if(tab_value==5){
				img.setImageResource(R.drawable.bar_health);
				thisUrl = false;

			}else if(tab_value==6){
				img.setImageResource(R.drawable.bar_art);
				thisUrl = false;
			}


			if(!thisUrl){
				share_btn.setVisibility(View.VISIBLE);
				content	=	bundle.getString("content");
				web.loadData("<!DOCTYPE html><head><meta charset=\"UTF-8\"/><meta name=\"viewport\" content=\"width=device-width\" /><title>Madhuri Dixit-Nene</title><link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"http://madhuridixit-nene.com/wp/wp-content/themes/twentyeleven/style.css\" /><style type=\"text/css\">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style type=\"text/css\">p {padding:5px !important;margin:5px !important;}</style></head><body class=\"single single-post postid-1606 single-format-standard single-author singular two-column right-sidebar\"><div class=\"entry-content\">"+content+"</div></body></html>","text/html; charset=UTF-8", null);
				shareUrl=bundle.getString("shareUrl");
			}else{
				share_btn.setVisibility(View.GONE);
				movYear		=	bundle.getString("movieYear");
				movName		=	bundle.getString("movieName");
				movImage		=	bundle.getString("movieImage");
				movAwards		=	bundle.getString("movieAwards");
				movDirector	=	bundle.getString("movieDirector");
				movProducer	=	bundle.getString("movieProducer");
				movActedAs	=	bundle.getString("movieActedAs");
				movAnecdotes	=	bundle.getString("movieAnecdotes");
				movSynopsis	=	bundle.getString("movieSynopsis");

				web.loadData("<p style='text-align:center'><img class='aligncenter size-full wp-image-1607' title='' src="+movImage+" alt='' width='240px' height='180px' /></p><p><center><U><H2>"+movName+"("+movYear+")</H2></U></center></p><p><strong>Director : </strong>"+movDirector+"</p><p><strong>Producer : </strong>"+movProducer+"</p><p><strong>Character : </strong>"+movActedAs+"</p><p><strong>Summary : </strong>"+movSynopsis+"</p><p><strong>Synopsis : </strong>"+movAnecdotes+"</p>\n","text/html; charset=UTF-8", null);
				shareUrl=bundle.getString("shareUrl");
			}
		}

		bckimg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
				web.stopLoading(); 
				web.loadUrl("");
				web.reload();
				web = null;
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				/*if(tab_value==1){
					PushService.setDefaultPushCallback(context, LatestTab.class);
				}else if(tab_value==2){
					PushService.setDefaultPushCallback(context, DanceTab.class);
				}else if(tab_value==3){
					PushService.setDefaultPushCallback(context, MoviesTab.class);
				}else if(tab_value==4){
					PushService.setDefaultPushCallback(context, FashionTab.class);
				}else if(tab_value==5){
					PushService.setDefaultPushCallback(context, HealthTab.class);
				}else if(tab_value==6){
					PushService.setDefaultPushCallback(context, ArtTab.class);
				}*/
				finish();
				overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			}
		});

		share_btn.setOnClickListener(new OnClickListener() {

			@Override


			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Madhuri");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
				startActivity(Intent.createChooser(sharingIntent, "Share via"));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_web_view, menu);
		return false;
	}

	class MyWebViewClient extends WebViewClient {
		@Override
		// show the web page in webview but not in web browser
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			try
			{
			view.getSettings().setJavaScriptEnabled(true);
			view.getSettings().setPluginsEnabled(true);
			view.getSettings().setPluginState(PluginState.ON);
			view.getSettings().setAllowFileAccess(true);
			view.getSettings().setBuiltInZoomControls(true);
			view.getSettings().setBlockNetworkLoads(false);
			
			
			view.loadUrl(url);
			
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			return true;
		}
		
		

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			showProgress.clearAnimation();
			showProgress.setVisibility(View.GONE);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			showProgress.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate));
			showProgress.setVisibility(View.VISIBLE);
		}
		
		
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		try
		{
		web.stopLoading(); 
		web.loadUrl("");
		web.reload();
		web = null;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		/*if(tab_value==1){
			PushService.setDefaultPushCallback(context, LatestTab.class);
		}else if(tab_value==2){
			PushService.setDefaultPushCallback(context, DanceTab.class);
		}else if(tab_value==3){
			PushService.setDefaultPushCallback(context, MoviesTab.class);
		}else if(tab_value==4){
			PushService.setDefaultPushCallback(context, FashionTab.class);
		}else if(tab_value==5){
			PushService.setDefaultPushCallback(context, HealthTab.class);
		}else if(tab_value==6){
			PushService.setDefaultPushCallback(context, ArtTab.class);
		}*/
		finish();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		/*	super.onBackPressed();*/

	}
}
