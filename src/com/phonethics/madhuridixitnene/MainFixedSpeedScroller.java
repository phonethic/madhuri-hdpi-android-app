package com.phonethics.madhuridixitnene;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Handler;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class MainFixedSpeedScroller  extends Scroller {

	private int mDuration = 2200;

	public MainFixedSpeedScroller(Context context) {
		super(context);
	}

	public MainFixedSpeedScroller(Context context, Interpolator interpolator) {
		super(context, interpolator);
	
	}

	@TargetApi(11)
	public MainFixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
		super(context, interpolator, flywheel);
	}


	@Override
	public void startScroll(int startX, int startY, int dx, int dy, int duration) {
		// Ignore received duration, use fixed one instead
		super.startScroll(startX, startY, dx, dy, mDuration);
		
	}

	@Override
	public void startScroll(int startX, int startY, int dx, int dy) {
		// Ignore received duration, use fixed one instead
		super.startScroll(startX, startY, dx, dy, mDuration);
		
	}
}
